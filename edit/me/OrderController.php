<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Http\Requests\Backend\OrderCreateRequest;
use App\Http\Requests\Backend\OrderUpdateRequest;
use Cache;

class OrderController extends Controller
{
    /**
     * Show orders
     *
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Order order)
    {
        return view('backend.orders.index', compact('data'));
    }

    /**
     * Create orders
     *
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Order $model)
    {
        return view('backend.orders.form', compact('model'));
    }

    /**
     * Store orders
     *
     * @param  \App\Http\Controllers\Backend\OrderCreateRequest $request
     * @return  \Illuminate\Http\RedirectResponse
     */
    public function store(OrderCreateRequest $request)
    {
        if ($request->validated()) {
            $request->store();
            return back();
        }
    }

    /**
     * Edit orders
     *
     * @param  \App\Models\Order $model
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Order $model,$id)
    {
        return view('backend.orders.form', compact('model', 'id'));
    }

    /**
     * Update orders
     *
     * @param  \App\Http\Requests\Backend\OrderUpdateRequest $request
     * @param  $id
     * @return  \Illuminate\Http\RedirectResponse
     */
    public function update(OrderUpdateRequest $request, $id)
    {
        if ($request->validated()) {
            $request->store($id);
            return back();
        }
    }

    /**
     * Destroy orders
     *
     * @param  \App\Models\Order $model
     * @return  \Illuminate\Http\RedirectResponse
     * @throws  \Exception
     */
    public function destroy(Order $model)
    {
        $model->delete();
        return back();
    }
}
