<?php

namespace App\System\Repositories;

use App\System\Contracts\BrandRepositoryInterface;
use App\Models\Brand;
use App\System\Exceptions\GeneralException;

class BrandRepository extends BaseRepository implements BrandRepositoryInterface
{
    private $cacheName = 'brands';
    private $cacheTime = 1;

    public function __construct(Brand $brand)
    {
        parent::__construct($brand);
    }

    /**
     * Brand slug with brand array
     *
     * @return object
     * @throws \Exception
     */
    public function brandSlugWithId(): object
    {
        return cache()->remember($this->cacheName, $this->cacheTime, function () {
            return $this->model->get()->mapWithKeys(function ($brand) {
                return [$brand->slug => $brand->id];
            });
        });
    }

}
