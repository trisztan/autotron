<?php

namespace App\System\Repositories;

use App\System\Contracts\ModelRepositoryInterface;
use App\Models\Models;
use App\System\Exceptions\GeneralException;

class ModelRepository extends BaseRepository implements ModelRepositoryInterface
{
    private $cacheName = 'models';
    private $cacheTime = 1;

    public function __construct(Models $models)
    {
        parent::__construct($models);
    }

    public function brandIdWithModelId(): object
    {
        return cache()->remember($this->cacheName, $this->cacheTime, function () {
            return $this->model->get()->mapWithKeys(function ($model) {
                return [$model->brand_id . "#" . $model->slug => $model->id];
            });
        });
    }

    /**
     * @param Brand $brand
     * @return bool
     * @throws \Exception
     */
    public function destroy(Models $models)
    {
        if (!$models->delete()) {
            throw new GeneralException();
        }

        return true;
    }
}
