<?php

namespace App\System\Repositories;

use App\System\Contracts\SearchRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SearchRepository implements SearchRepositoryInterface
{
    protected $cacheTime = 1;
    protected $key;
    protected $uri;
    protected $cache;
    protected $search = [];

    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    public function result($search)
    {

        if (empty($search)) {
            return false;
        }
        $this->key = md5(serialize($search));
       /* if ($this->cache::has($this->key)) {
            return $this->cache::get($this->key);
        }*/

        if (!is_array($search)) {
            $search = ltrim($search, '/');
            $search = explode('/', $search);
        }
        $result = [];
        foreach ($search as $uri) {
            $uri = explode('-', $uri, 2);

            if ($uri[0] == 'brands') {
                $types = explode(',', $uri[1]);

                foreach ($types as $type) {
                    $brand = explode('+', $type, 2);
                    $car = $brand[0];
                    if (!isset($brand[1])) {
                        $result['brands'][$car] = true;
                        continue;
                    }
                    $models = explode('+', $brand[1]);

                    foreach ($models as $model) {
                        $result['brands'][$car][$model] = $model;
                    }
                }
                continue;
            }

            $result[$uri[0]] = explode(',', $uri[1]);
        }

       /* $this->cache::add($this->key, $this->search, $this->cacheTime);*/
        return $result;
    }

}