<?php

namespace App\System\Repositories;

use App\Models\Vehicle;
use App\System\Contracts\VehicleRepositoryInterface;
use Illuminate\Support\Str;
use App\Models\VehicleValues;
use DB;
use App\System\Requests\VehicleStoreRequest;
use Auth;

class VehicleRepository extends BaseRepository implements VehicleRepositoryInterface
{

    public function __construct(Vehicle $vehicle)
    {
        parent::__construct($vehicle);
    }

    /**
     * Search by slug
     * FIXME: Refactor
     * @param bool $attrs
     * @return array
     */
    public function searchByUri($attrs = false): array
    {
        $vehicles = Vehicle::with('images', 'datas', 'datas.attribute');
        foreach ($attrs as $attr) {
            if ($attr['attribute'] == 'brand') {
                $vehicles = $vehicles->orwhere(function ($query) use ($attrs, $attr) {

                    if ($attr['value'] == null) {
                        $vehicles = $query->where('brand_id', $attr['id']);
                    } else {
                        $vehicles = $query->where('brand_id', $attr['id'])->where('model_id', $attr['value']);
                    }
                    foreach ($attrs as $carattribute) {
                        if ($carattribute['attribute'] != 'brand') {
                            $vehicles = $vehicles->whereHas('datas', function ($query) use ($carattribute) {
                                $expression = '=';
                                if (strpos($carattribute['attribute'], '_from') !== false) {
                                    $expression = '>=';
                                } else if (strpos($carattribute['attribute'], '_to') !== false) {
                                    $expression = '<=';
                                }

                                if (is_array($carattribute['value'])) {
                                    $query->where('attribute_id', '=', $carattribute['id'])->whereIn(\DB::raw('CAST(data as SIGNED)'), $carattribute['value']);
                                } else {
                                    $query->where('attribute_id', '=', $carattribute['id'])->where(\DB::raw('CAST(data as SIGNED)'), $expression, $carattribute['value']);
                                }
                            });
                        }
                    }
                    return $vehicles;
                });
            }
        }

        return ['count' => $vehicles->count(), 'vehicles' => $vehicles->get(),'sql' => $vehicles->toSql(),'bindings'=>$vehicles->getBindings()];
    }

    /**
     * Find car by id
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function find($id)
    {
        return $this->model::with(['images', 'values', 'group.features.attributes.values'])->where('vehicles.uuid', $id)->first();
    }

    /**
     * Find by author
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function findByAuthor($id)
    {
        return $this->model::with(['images', 'values', 'group.features.attributes.values'])->where('vehicles.created_by', $id)->get();
    }

    /**
     * Find by author and uuid
     *
     * @param $id
     * @param $uuid
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function findByAuthorAndUuid($id,$uuid)
    {
        return $this->model::with(['images'])->where('vehicles.created_by',$id)->where('vehicles.uuid',strtolower($uuid));
    }

    /**
     * Store a new car
     * FIXME: Refactor
     *
     * @param VehicleStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleStoreRequest $request)
    {
      DB::beginTransaction();

        try {

            $vehicle = new $this->model;
            $vehicle->group_id = 1;
            $vehicle->brand_id = request()->get('brand')[0];
            $vehicle->model_id = request()->get('model')[0];
            $vehicle->uuid = strtolower(Str::random(6));
            $vehicle->title = strtolower(Str::random(6));
            $vehicle->slug = str_slug($vehicle->title);
            $vehicle->created_by = Auth::user()->id;
            $vehicle->save();

            $requests = $request->all();
            unset($requests['brand'], $requests['model']);

            $attributes = [];

            foreach ($requests as $key => $value) {
                $attributes[] = new VehicleValues([
                    'attribute_id' => $key,
                    'vehicle_id' => $vehicle->id,
                    'data' => $value
                ]);
            }
            $vehicle->datas()->saveMany($attributes);
           DB::commit();
            return response()->json(['type' => 'success','url'=>url('dashboard/images/'.$vehicle->uuid)], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Hiba történt a feltöltés nem sikerült.', 'type' => 'error','error'=>$e], 200);
        }
    }
}
