<?php

namespace App\System\Repositories;

use App\System\Contracts\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseRepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function query()
    {
        return $this->model->newQuery();
    }

    public function search($query, $callback = null)
    {
        return $this->model->search($query, $callback);
    }

    public function select(array $columns = ['*'])
    {
        return $this->query()->select($columns);
    }

    public function make(array $attributes = [])
    {
        return $this->query()->make($attributes);
    }
}
