<?php

namespace App\System\Repositories;

use App\System\Contracts\GroupRepositoryInterface;
use App\Models\Group;
use App\System\Exceptions\GeneralException;

class GroupRepository extends BaseRepository implements GroupRepositoryInterface
{
    public function __construct(Group $group)
    {
        parent::__construct($group);
    }

    /**
     * @param Group $group
     * @return bool
     * @throws \Exception
     */
    public function destroy(Group $group)
    {
        if (! $group->delete()) {
            throw new GeneralException();
        }

        return true;
    }
}