<?php
namespace App\System\Repositories;

use App\System\Contracts\ImageRepositoryInterface;

class ImageRepository implements ImageRepositoryInterface
{
    /**
     * Get uploaded image
     *
     * @param $uri
     * @return string
     */
    public function get($uri):string
    {
        $path = storage_path('app/public/' . $uri);
        if (file_exists($path)) {
            return file_get_contents($path);
        }
        return false;
    }
}
