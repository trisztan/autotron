<?php

namespace App\System\Contracts;

interface BrandRepositoryInterface
{
    public function brandSlugWithId(): object;
}