<?php
namespace App\System\Contracts;

interface ModelRepositoryInterface
{
    public function brandIdWithModelId():object;
}
