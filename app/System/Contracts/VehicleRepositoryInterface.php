<?php

namespace App\System\Contracts;

use App\System\Requests\VehicleStoreRequest;

interface VehicleRepositoryInterface
{
    public function searchByUri($search): array;

    public function find($uuid);

    public function store(VehicleStoreRequest $request);

    public function findByAuthor($id);

}
