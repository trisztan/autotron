<?php
namespace App\System\Contracts;

interface BaseRepositoryInterface
{

    public function query();

    public function search($query, $callback = null);

    public function select(array $columns = ['*']);

    public function make(array $attributes = []);
}
