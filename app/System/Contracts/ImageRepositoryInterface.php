<?php
namespace App\System\Contracts;

interface ImageRepositoryInterface
{
    public function get($uri):string;
}