<?php
namespace App\System\Contracts;

interface SearchRepositoryInterface
{
    public function result($search);

}