<?php

namespace App\System\Providers;

use App\System\Repositories\GroupRepository;
use App\System\Contracts\GroupRepositoryInterface;

use App\System\Repositories\SearchRepository;
use App\System\Contracts\SearchRepositoryInterface;

use App\System\Repositories\VehicleRepository;
use App\System\Contracts\VehicleRepositoryInterface;

use App\System\Repositories\ImageRepository;
use App\System\Contracts\ImageRepositoryInterface;

use App\System\Repositories\BrandRepository;
use App\System\Contracts\BrandRepositoryInterface;

use App\System\Repositories\ModelRepository;
use App\System\Contracts\ModelRepositoryInterface;

use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(GroupRepositoryInterface::class, GroupRepository::class);
        $this->app->bind(SearchRepositoryInterface::class, SearchRepository::class);
        $this->app->bind(VehicleRepositoryInterface::class, VehicleRepository::class);
        $this->app->bind(ImageRepositoryInterface::class, ImageRepository::class);
        $this->app->bind(BrandRepositoryInterface::class, BrandRepository::class);
        $this->app->bind(ModelRepositoryInterface::class, ModelRepository::class);
    }
}
