<?php

namespace App\System\Utils;

class Helper
{
    public static function image($image)
    {
        return url('images/' . $image);
    }

    public static function money($string)
    {
        $string = explode(' ', $string);
        return number_format((int)$string[0], 0, ',', ' ') . " " . $string[1];
    }

    public static function weight($weight)
    {
        return number_format($weight, 1, ',', ' ') . " Kg";
    }
}
