<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Group;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function models(Request $request)
    {
        $html = view('frontend.search.models',compact('groups'))->render();
        return response()->json(['status'=>200,'data']);
    }
}
