<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\System\Contracts\GroupRepositoryInterface;
use App\System\Contracts\VehicleRepositoryInterface;
use App\System\Requests\VehicleStoreRequest;
use Illuminate\Http\Request;
use Cache;
use Image;
use Auth;

class DashboardController extends Controller
{

    public function index(Request $request)
    {
        return view('frontend.dashboard.index');
    }

    public function upload(Request $request)
    {
        $groups = app(GroupRepositoryInterface::class);
        $groups = $groups->query()->get();

        return view('frontend.dashboard.upload', compact('groups'));
    }

    public function store(VehicleStoreRequest $request)
    {
        $vehicles = app(VehicleRepositoryInterface::class);
        $result = $vehicles->store($request);
        if ($result) {
            return response()->json(['success' => true,'data'=>$result]);
        }
        return response()->json(['success' => false]);
    }

    public function list()
    {
        $vehicles = app(VehicleRepositoryInterface::class);
        $vehicles = $vehicles->findByAuthor(Auth::user()->id);
        return view('frontend.dashboard.list', compact('vehicles'));
    }

    public function images(Request $request)
    {

        $vehicles = app(VehicleRepositoryInterface::class);
        $vehicles = $vehicles->findByAuthorAndUuid(Auth::user()->id,$request->uuid);
        return $vehicles->get();
        if(!$vehicles->exists()){
            abort(404);
        }

        return view('frontend.dashboard.images', compact('vehicles'));
    }

    public function notifications()
    {
        return view('frontend.dashboard.notifications');
    }

    public function settings()
    {
        return view('frontend.dashboard.settings');
    }
}
