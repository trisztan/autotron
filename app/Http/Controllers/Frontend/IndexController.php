<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\System\Contracts\SearchRepositoryInterface;
use App\System\Contracts\GroupRepositoryInterface;
use App\System\Contracts\VehicleRepositoryInterface;
use App\System\Contracts\ImageRepositoryInterface;
use Illuminate\Http\Request;
use Cache;
use Image;
use App\Models\Vehicle;
class IndexController extends Controller
{
    /**
     * Home Page
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $search = app(SearchRepositoryInterface::class);
        $groups = app(GroupRepositoryInterface::class);

        $search = $search->result($request->segments());
        $groups = $groups->query()->get();

        return view('frontend.index', compact('groups', 'search'));
    }

    public function vehicles(Request $request)
    {
        $vehicles = app(VehicleRepositoryInterface::class);

        if ($request->has('search'))
        {
            $result = $vehicles->searchByUri($request->search);
        } else {
            $result['vehicles'] = $vehicles->query()->get();
            $result['count'] = $vehicles->query()->count();
        }

        $html = view('frontend.search.card', [
            'count' => $result['count'],
            'vehicles' => $result['vehicles']
        ])->render();

        return response()->json(compact('count', 'html'), 200);
    }

    public function vehicle(Request $request)
    {
        $vehicle = app(VehicleRepositoryInterface::class);
        $vehicle = $vehicle->find($request->uuid);

        return view('frontend.vehicle', compact('vehicle'));
    }

    public function image(Request $request)
    {
        $image = app(ImageRepositoryInterface::class);
        $content = $image->get($request->image);
        if (!$content) {
            return abort(404);
        }
        return response($content, 200)->header('Content-Type', 'image/jpg');
    }
}
