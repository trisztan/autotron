<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'=>'required',
            'lastname'=>'required',
            'email'=>'required|email',
            'phone'=>'required',
            'country'=>'required',
            'zip'=>'required',
            'city'=>'required',
            'street'=>'required',
            'delivery_firstname'=>'required_if:other_delivery,on',
            'delivery_lastname'=>'required_if:other_delivery,on',
            'delivery_phone'=>'required_if:other_delivery,on',
            'delivery_country'=>'required_if:other_delivery,on',
            'delivery_zip'=>'required_if:other_delivery,on',
            'delivery_city'=>'required_if:other_delivery,on',
            'delivery_street'=>'required_if:other_delivery,on',
            'delivery'=>'required',
            'accept'=>'required',
        ];
    }
}
