<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Product;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->is_admin == 0){
            abort(404);
        }
        return $next($request);
    }
}
