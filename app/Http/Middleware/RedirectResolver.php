<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Redirect;

class RedirectResolver
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $redirect = Redirect::whereFrom($request->path());
        if($redirect->exists()){
            return redirect($redirect->first()->to,$redirect->first()->code);
        }
        return $next($request);
    }
}
