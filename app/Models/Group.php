<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Group extends Model
{
    use HasTranslations;
    public $translatable = ['title', 'slug'];

    public function features()
    {
        return $this->hasMany('App\Models\Feature');
    }

    public function brands()
    {
        return $this->hasMany('App\Models\Brand');
    }
}
