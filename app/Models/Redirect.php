<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $appends = ['redirect_label'];
    /*
    |--------------------------------------------------------------------------
    | STATIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /**
     * Get Types
     *
     * @return array
     */
    public static function getTypes()
    {
        return [
            301 => '301 - Végleges átirányítás',
            302 => '302 - Átmeneti átirányítás'
        ];
    }

    public function getRedirectLabelAttribute()
    {
        return self::getTypes()[$this->code];
    }
}
