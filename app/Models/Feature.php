<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Feature extends Model
{
    use HasTranslations;
    public $translatable = ['title'];

    public function attributes()
    {
        return $this->hasMany('App\Models\Attribute');
    }
}
