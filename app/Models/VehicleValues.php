<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleValues extends Model
{
    protected $with = ['attribute.values'];
    protected $fillable = ['attribute_id','vehicle_id','data'];
    protected $casts = [
        'data' => 'string',
    ];
    /**
     * Get the comments for the blog post.
     */
    public function attribute()
    {
        return $this->hasOne('App\Models\Attribute','id','attribute_id');
    }
}
