<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Attribute extends Model
{
    use HasTranslations;
    public $translatable = ['title', 'slug'];

    public function values()
    {
        return $this->hasMany('App\Models\AttributeValue');
    }

    public function feature()
    {
        return $this->belongsTo('App\Models\Feature');
    }
}
