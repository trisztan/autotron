<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\System\Utils\Helper;

class Vehicle extends Model
{

    //protected $with = ['group.features.attributes.values','datas.attribute.values'];

    protected $appends = ['url'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getImage()
    {
        if(!isset(self::images()->first()->path)){
            // SHIT ide kell kép
            return false;
        }
        return Helper::image(self::images()->first()->path);
    }

    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }

    public function values()
    {
        return $this->hasMany('App\Models\VehicleValues','vehicle_id');
    }

    public function owner()
    {
        return $this->belongsTo('App\Models\Users','vehicle_id');
    }
    /**
     * Get the comments for the blog post.
     */
    public function datas()
    {
        return $this->hasMany('App\Models\VehicleValues');
    }

    /**
     * Get all of the post's comments.
     */
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    /**
     * Get all of the post's comments.
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand');
    }

    /**
     * Get all of the post's comments.
     */
    public function model()
    {
        return $this->belongsTo('App\Models\Models');
    }

    /**
     *
     */
    public function getUrlAttribute()
    {
        return "vehicle/".$this->uuid;
    }

    public function getAttr($what, $title)
    {
        $value = $this->datas->map(function ($attribute) use ($what, $title) {

            if ($what == $attribute['attribute']['slug']) {
                $suffix = null;
                if ($attribute['attribute']['type'] == 'number') {
                    $suffix = $attribute['attribute']['suffix'] ?? null;
                    $value = $attribute['data'] ?? 0;
                }
                if ($attribute['attribute']['type'] == 'select') {
                    $where = $attribute['data'];
                    $values = $attribute['attribute']['values'];

                    $value = collect($values)->map(function ($values) use ($where) {
                        if ($values['id'] == $where) {
                            return (string)$values['title'];
                        }
                    })->reject(function ($attribute) {
                        return empty($attribute);
                    });
                    $value = array_values($value->toArray())[0];
                }
                if (!$title) {
                    return $value . ' ' . $suffix;
                }

                return [$attribute['attribute']['title'] => $value . ' ' . $suffix];
            }
        })->reject(function ($attribute) {
            return empty($attribute);
        });

        return (string)array_values($value->toArray())[0];
    }
}
