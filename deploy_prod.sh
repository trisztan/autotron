#!/bin/sh
cd /var/www/vhosts/basicauth.com/motor.basicauth.com/
export PATH=/opt/plesk/php/7.1/bin:$PATH
git fetch --all
git reset --hard origin/master
php artisan down

echo "Run Composer Install"
composer install
echo "Build frontend css & js"
yarn install
yarn run build
echo "Clear cache"

php artisan cache:clear

echo "Run migrations"
php artisan migrate --force

echo "Fix permissions"
# Fix permissions
cd /var/www/vhosts/basicauth.com/motor.basicauth.com/
chown basicauth:psacln -R *
chmod 777 -R storage
find . -type f -exec chmod 755 {} +
find . -type f -exec chmod 655 {} +

php artisan up
