<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Models\User;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function loginWithFakeUser()
    {
        $user = new User();
        $user->id = 1;
        $user->name = 'User';
        $user->is_admin = 0;
        return $this->be($user);
    }

    public function loginWithFakeAdmin()
    {
        $admin = new User();
        $admin->id = 2;
        $admin->name = 'Admin';
        $admin->is_admin = 1;
        return $this->be($admin);
    }
}
