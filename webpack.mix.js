const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery','easy-autocomplete','easyAutocomplete'],
    easyAutocomplete: ['easy-autocomplete','easyAutocomplete'],
    summernote: ['summernote'],
    Dropzone : ['Dropzone'],
    toastr : ['toastr'],
    select2 : ['select2'],
    chart : ['chart'],
});

mix.js([
    './node_modules/jquery/dist/jquery.js',
    './node_modules/bootstrap/dist/js/bootstrap.min.js',
    './node_modules/summernote/dist/summernote-bs4.js',
    './node_modules/summernote/lang/summernote-hu-HU.js',
    './node_modules/jstree/dist/jstree.min.js',
    './node_modules/dropzone/dist/min/dropzone.min.js',
    './node_modules/toastr/build/toastr.min.js',
    './node_modules/chart.js/Chart.js',
    './resources/js/backend/app.js'],
    'public/js/backend/app.js')
    .sass('resources/sass/backend/app.scss', 'public/css/backend/');

mix.js([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js',
        './node_modules/easy-autocomplete/dist/jquery.easy-autocomplete.js',
        './node_modules/select2/dist/js/select2.full.min.js',
        './node_modules/dropzone/dist/min/dropzone.min.js',
        './resources/js/frontend/app.js'],
    'public/js/frontend/app.js')
   .sass('resources/sass/frontend/app.scss', 'public/css/frontend/');

mix.js([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/icheck/icheck.min.js',
        './resources/js/backend/login.js'],
    'public/js/backend/login.js');

mix.browserSync({
    host: 'autokereso.loc',
    proxy: 'http://autokereso.loc',
    port: 4111
});
