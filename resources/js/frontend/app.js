require('bootstrap');
global.$ = global.jQuery = require('jquery');
import 'select2';
require('jquery');
require('datatables.net-bs4');
require('datatables.net-responsive-bs4');

var App = function () {
    'use strict';
    var searchLoaded = false;

    var verifyToken = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    var selectMulti = function () {
        $('.selectMulti').select2({
            width:'100%',
            closeOnSelect : false,
            allowHtml: true,
            allowClear: true,
        });
    }

    var showModels = function () {
        var brands = $('.selectBrand').val();
        $('.models').hide();
        if (searchEnable == true) {
            $.each(brands, function (i, val) {
                $('.load' + val).show();

                $('.load' + val).find('.selectModel').select2();
            });
        } else {
            $('.loadModels').find('.selectModel').attr('data-show',false);
            $('.load' + brands).show();
            $('.load' + brands).find('.selectModel').attr('data-show',true);
            $('.load' + brands).find('.selectModel').select2({
                closeOnSelect : false,
                allowHtml: true,
                allowClear: true,
            });
        }
    }

    var selectBrand = function () {

        $('.selectBrand').select2({
            closeOnSelect : false,
            allowHtml: true,
            allowClear: true,
        }).on("change", function () {
            showModels();
            search();
        });
    }

    var selectModel = function () {
        $('.selectModel').select2({
            closeOnSelect : false,
            allowHtml: true,
            allowClear: true,
        }).on("change", function () {
            search();
        });
    }

    var search = function () {
        if ($('.filter').length) {
            var urls = [];
            var attributes = [];
            var brands = $('.selectBrand').val();
            var models = [];
            var attributes = [];

            if (brands.length) {
                $.each(brands, function (i, val) {
                    var value = $("[data-brand='" + val + "']").val();

                    if (value != '') {
                        models.push([val, value]);
                    } else {
                        models.push([val]);
                    }
                });
            }
            if (brands.length) {
                $.each(brands, function (i, val) {
                    var value = $("[data-brand='" + val + "']").val();
                    var brand = $("[data-brand='" + val + "']").attr('data-brand-id');

                    if (value != '') {
                        var values = $("[data-brand='" + val + "']").find('option:selected').map(function () {
                            return $(this).attr('data-uuid');
                        }).get();
                        $.each(values, function (i, val) {
                            attributes.push({'id': brand, 'attribute': 'brand', 'value': val});
                        });
                    } else {
                        attributes.push({'id': brand, 'attribute': 'brand', 'value': null});
                    }
                });
            }

            var brands = [];
            $.each(models, function (i, val) {
                if (val[1]) {
                    var link = val[1].join("+");
                    brands.push(val[0] + "+" + link);
                } else {
                    brands.push(val[0]);

                }
            });

            if (brands.length > 0) {
                urls.push('brands-' + brands)
            }

            $(".filter :input:visible").each(function (e) {
                if (this.value != 0 && this.value != '') {
                    if ($(this).is('[multiple]')) {
                        var value = $(this).val();
                    } else {
                        var value = this.value;
                    }
                    var key = $(this).attr('name').toString().replace("[]", "");
                    urls.push(key + '-' + value);
                }
            });


            $(".filter :input:visible").each(function (e) {
                if ($(this).attr('name')) {
                    var attribute = $(this).attr('name').toString().replace("[]", "");
                    var type = $(this).attr('type').toString();
                    var id = $(this).attr('data-attribute');

                    if (type == 'number' && this.value != 0 && this.value !== undefined) {
                        attributes.push({'id': id, 'attribute': attribute, 'value': this.value});
                    }
                    if (type == 'select' && this.value != 0 && this.value !== undefined) {
                        attributes.push({
                            'id': id,
                            'attribute': attribute,
                            'value': $(this).find('option:selected').attr('data-uuid')
                        });
                    }
                    if (type == 'selectmulti') {
                        var values = $(this).find('option:selected').map(function () {
                            return $(this).attr('data-uuid');
                        }).get();
                        if (values.length) {
                            attributes.push({'id': id, 'attribute': attribute, 'value': values});
                        }
                    }
                }
            });

            var link = '/' + urls.join("/");
            loadCars(attributes);
            window.history.replaceState(null, null, link);
        }
    }
    var searchDetect = function () {
        if ($('.filter').length) {

            $("input").on("change paste keyup", function () {
                search();
            });
            $('select').change(function (e) { // select element changed
                search();
            });
        }
    }
    var loadCars = function (attributes = false) {
        $.ajax({
            url: '/vehicles',
            type: "POST",
            method: "POST",
            dataType: "json",
            data: {
                "search": attributes,
            },
            cache: false,
            beforeSend: function () {
                /*btn.button('loading');*a/
            },
            complete: function () {
                /*  btn.button('reset');*/
            },
            success: function (result) {
                $('[data-cars]').html(result.html);
                /*  $('[data-vehicle-count]').html(result.count);
                  $('[data-vehicle-count]').each(function () {
                      $(this).prop('[data-vehicle-count]',0).animate({
                          Counter: $(this).text()
                      }, {
                          duration: 1000,
                          easing: 'swing',
                          step: function (now) {
                              $(this).text(Math.ceil(now));
                          }
                      });
                  });*/
            }
        });
    }
    var searchInit = function () {
        if (!searchLoaded) {
            search();
        }
        searchLoaded = true;
    }
    var setDatatableDefaults = function() {
        $.extend( true, $.fn.dataTable.defaults, {
            responsive: true,
            oLanguage: {
                "sEmptyTable": "Nincs rendelkezésre álló adat",
                "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
                "sInfoEmpty": "Nulla találat",
                "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
                "sInfoPostFix": "",
                "sInfoThousands": " ",
                "sLengthMenu": "_MENU_ találat oldalanként",
                "sLoadingRecords": "Betöltés...",
                "sProcessing": "Feldolgozás...",
                "sSearch": "Keresés:",
                "sZeroRecords": "Nincs a keresésnek megfelelő találat",
                "oPaginate": {
                    "sFirst": "Első",
                    "sPrevious": "Előző",
                    "sNext": "Következő",
                    "sLast": "Utolsó"
                },
                "oAria": {
                    "sSortAscending": ": aktiválja a növekvő rendezéshez",
                    "sSortDescending": ": aktiválja a csökkenő rendezéshez"
                }
            },
        } );
    }
    var vehicles = function () {
        if ($.fn.DataTable.isDataTable($('#vehiclesTable'))) {
            $('#vehiclesTable').DataTable().destroy();
        }
        var vehicles = $('#vehiclesTable').DataTable();
    }
    var uploadValidator = function () {

        var drop = $('#uploadFile');
        drop.dropzone({
            paramName: "file",
            uploadMultiple: true,
            parallelUploads: 10,
            maxFilesize: 8,
            maxFiles: 10,
            thumbnail: function(file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                }
            },
            acceptedFiles: ".jpg",
            autoProcessQueue: false,
            addRemoveLinks: true,
            keepLocal:true,
            init: function() {
                this.on('addedfile', function(file){
                    var preview = document.getElementsByClassName('dz-preview');
                    preview = preview[preview.length - 1];
                    var imageName = document.createElement('span');
                    imageName.innerHTML = file.name;
                    preview.insertBefore(imageName, preview.firstChild);
                });
            }
        });

        $(document).on('click focusout blur', '.form-group,.nav-link,.nav-item', function () {

            $('.invalid-feedback').remove();
            $('.valid-feedback').remove();
            $('.is-invalid').removeClass('is-invalid');
            $('.is-valid').removeClass('is-valid');
            var form = $(".upload-form :input");
            var count = 0;
            var valid = 0;
            var length = form.length;
            window.adat = {};
            form.each(function (index, element) {
                var name = $(this).attr('name');
                var show = $(this).attr('data-show');
                var required = $(this).attr('data-required');

                if (show === 'true' && name) {
                    var id = $(this).attr('data-attribute');
                    var id = id ? id : $(this).attr('data-name');
                    if(required === 'yes'){
                        count++;
                    }
                    var type = $(this).attr('type');
                    if (type == 'number' || type == 'select') {
                        if (this.value == '' || this.value == 0 || this.value === undefined) {
                            $(this).addClass('is-invalid');
                            $(this).parent().find('.input-group-text').addClass('is-invalid');
                            $(this).parent().append('<div class="invalid-feedback">A mező kitöltése kötelező</div>')
                        } else {
                            $(this).addClass('is-valid');
                            $(this).parent().find('.input-group-text').addClass('is-valid');
                            $(this).parent().append('<div class="valid-feedback"></div>');
                            if(type == 'select'){
                                var value = $(this).find('option:selected').attr('data-uuid');
                            }
                            else {
                                var value = this.value;
                            }
                            adat[id] = value;
                            valid++;
                        }
                    }

                    if (type == 'selectMulti') {
                        if (this.value == '' || this.value == 0 || this.value === undefined) {
                            $(this).parent().find('.select2-selection--single').addClass('is-invalid');
                            $(this).parent().append('<div class="invalid-feedback">A mező kitöltése kötelező</div>')
                        } else {
                            $(this).parent().find('.select2-selection--single').addClass('is-valid');
                            $(this).parent().append('<div class="valid-feedback"></div>');
                            var values = $(this).find('option:selected').map(function () {
                                return $(this).attr('data-uuid');
                            }).get();
                            adat[id] = values;
                            if(required === 'yes'){
                                valid++;
                            }
                        }
                    }
                }
                if (index === (length - 1)) {
                    var percent = Math.round(((100 / count) * valid));
                    $('.progress-bar').css({'width': percent + "%"}).attr('aria-valuenow', percent).html(percent + " %");
                    if (percent >= 100) {
                        $('[data-upload]').removeAttr('disabled')
                    } else {
                        $('[data-upload]').attr('disabled', true)
                    }
                }
            });

        });

        $(document).on('click', '[data-upload]', function () {
            console.log(window.adat);
            $.ajax({
                url: '/dashboard/store',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: window.adat,
                cache: false,
                success: function (result) {
                    if(result.type == 'success'){
                        location.replace(result.url);
                    }
                    else {
                        location.replace(result.url);
                    }
                }
            });
        });
    }
    var upload = function () {

    }
    var openCar = function() {
        $(document).on('click', '[data-href]',function (e) {
            var href = $(e.currentTarget).data('href');
            window.location = href;
        });
    }
    return {
        init: function () {
            verifyToken();
            selectMulti();
            selectBrand();
            selectModel();
            searchDetect();
            searchInit();
            uploadValidator();
            upload();
            openCar();
            setDatatableDefaults();
            vehicles();
        }
    };
}();

$(function () {
    App.init();
});
