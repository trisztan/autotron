require('datatables.net-bs4');
require('datatables.net-responsive-bs4');
require('./AdminLTE');
require('summernote');
import Chart from "chart.js";

window.toastr = toastr;
var App = function () {
    'use strict';

    var verifyToken = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
    var setDatatableDefaults = function() {
        $.extend( true, $.fn.dataTable.defaults, {
            responsive: true,
            oLanguage: {
                "sEmptyTable": "Nincs rendelkezésre álló adat",
                "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
                "sInfoEmpty": "Nulla találat",
                "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
                "sInfoPostFix": "",
                "sInfoThousands": " ",
                "sLengthMenu": "_MENU_ találat oldalanként",
                "sLoadingRecords": "Betöltés...",
                "sProcessing": "Feldolgozás...",
                "sSearch": "Keresés:",
                "sZeroRecords": "Nincs a keresésnek megfelelő találat",
                "oPaginate": {
                    "sFirst": "Első",
                    "sPrevious": "Előző",
                    "sNext": "Következő",
                    "sLast": "Utolsó"
                },
                "oAria": {
                    "sSortAscending": ": aktiválja a növekvő rendezéshez",
                    "sSortDescending": ": aktiválja a csökkenő rendezéshez"
                }
            },
        } );
    }
    var backendUsers = function () {
        if ($.fn.DataTable.isDataTable($('#users'))) {
            $('#users').DataTable().destroy();
        }
        var users = $('#users').DataTable();
    }
    var backendPages = function () {
        if ($.fn.DataTable.isDataTable($('#pages'))) {
            $('#pages').DataTable().destroy();
        }
        var pages = $('#pages').DataTable();
    }
    var backendCoupons = function () {
        if ($.fn.DataTable.isDataTable($('#coupons'))) {
            $('#coupons').DataTable().destroy();
        }
        var coupons = $('#coupons').DataTable();
    }
    var backendDelivery = function () {
        if ($.fn.DataTable.isDataTable($('#deliveries'))) {
            $('#deliveries').DataTable().destroy();
        }
        var deliveries = $('#deliveries').DataTable();
    }
    var backendOrders = function () {
        if ($.fn.DataTable.isDataTable($('#orders'))) {
            $('#orders').DataTable().destroy();
        }
        var orders = $('#orders').DataTable();
    }
    var backendOffers = function () {
        if ($.fn.DataTable.isDataTable($('#offer'))) {
            $('#offer').DataTable().destroy();
        }
        var offers = $('#offer').DataTable();
    }
    var summernote = function () {
        $('.summernote').summernote({
            height: 300,
            lang: 'hu-HU',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ol', 'ul', 'paragraph', 'height']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['undo', 'redo', 'fullscreen', 'codeview', 'help']]
            ],
            popover: {
                image: [],
                link: [],
                air: []
            }
        });
    }
    var backendCategories = function () {
        if ($.fn.DataTable.isDataTable($('#categories'))) {
            $('#categories').DataTable().destroy();
        }
        var categories = $('#categories').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/admin/categories/data',
            columns: [
                {data: 'fullslug'},
                {data: 'sku'},
                {data: 'part'},
                {data: 'manufacturer'},
                {data: 'brand'},
                {data: 'ccm'},
                {data: 'model'},
                {data: 'year'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    }
    var backendProducts = function () {
        var table = $('#productTable');
        if ($.fn.DataTable.isDataTable(table)) {
            table.DataTable().destroy();
        }
        var products = table.DataTable({
            serverSide: true,
            processing: true,
            ajax: '/admin/products/data',
            columns: [
                {data: 'image'},
                {data: 'sku'},
                {data: 'title'},
                {data: 'price'},
                {data: 'saleprice'},
                {data: 'weight'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    }
    var loadingButton = function () {
        $.fn.button = function (action) {
            if (action === 'loading' && this.data('loading-text')) {
                this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
                this.html(this.data('original-text')).prop('disabled', false);
            }
        };
    }
    var download = function () {

        $(document).on("click", "[data-export-start]", function () {
            var type = $(this).attr('data-export-start');
            var btn = $(this);
            var urls = new Array();
            var data = $('#tree_4').jstree("get_selected", true);
            $.each(data, function (key, value) {

                var url = value.id;
                if (typeof value.parents !== 'undefined' && value.parents.length > 0) {
                    $.each(value.parents, function (key, value) {
                        if (value != '#') {
                            url = value + "/" + url;
                        }
                    });
                }
                urls.push(url);
            });

            $.ajax({
                url: '/admin/export/create/' + type,
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    "urls": urls,
                },
                cache: false,
                beforeSend: function () {
                    btn.button('loading');
                },
                complete: function () {
                    btn.button('reset');
                },
                success: function (result) {
                    $('#files > tbody').append(result.html);
                    toastr.success('A fájl exportálva.');
                }
            });
        });
    }
    var importFile = function () {

        $(document).on("click", "[data-import]", function () {
            $('.card').find(':button').prop('disabled', true);
            var file = $(this).attr('data-import');
            var btn = $(this);
            $('[data-alert]').addClass('hide');
            $.ajax({
                url: '/admin/import/store',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    "file": file,
                },
                cache: false,
                beforeSend: function () {
                    btn.button('loading');
                },
                complete: function () {
                    btn.button('reset');
                },
                success: function (result) {
                    $('[data-alert]').find('.error').empty();
                    if (result.success == true) {
                        $('[data-import-status]').html('100% Importálás sikeres volt!');
                        $('.progress-bar').attr('aria-valuenow', 100);
                        $('.progress-bar').css({
                            'width': '100%'
                        });
                        toastr.success('A fájl importálásra sikeres!');

                    }
                    if (result.error && result.error.length > 0) {
                        $('[data-alert]').removeClass('hide');
                        $.each(result.error, function (index, value) {
                            $('[data-alert]').find('.error').append('<li>' + value + '</li>');
                        });
                    }
                }
            });
            setTimeout(function () {
                importStatus()
            }, 5000);
        });
    }
    var importStatus = function (count = 1) {

        $.ajax({
            url: '/admin/import/status',
            type: "POST",
            method: "POST",
            dataType: "json",
            data: {
                count: count
            },
            cache: false,
            success: function (result) {
                if (result == "") {
                    $('.card').find(':button').prop('disabled', false);
                }
                if (result.status == 'start') {
                    $('[data-import-status]').html("");
                    $('.progress-bar').attr('aria-valuenow', 0);
                    $('.progress-bar').css({
                        'width': '0%'
                    });
                }
                if (result.status == 'progress') {
                    $('[data-import-status]').html(result.totalrow + " / " + result.currentrow);
                    var percent = ((result.currentrow / result.totalrow) * 100);
                    $('.progress-bar').attr('aria-valuenow', percent);
                    $('.progress-bar').css({
                        'width': percent + '%'
                    });
                }
                if (result.status == 'start' || result.status == 'progress') {
                    setTimeout(function () {
                        importStatus()
                    }, 1000);
                }
                if (result.status == 'finished') {
                    $('[data-import-status]').html('100% Importálás sikeres volt!');
                    $('.progress-bar').attr('aria-valuenow', 100);
                    $('.progress-bar').css({
                        'width': '100%'
                    });
                    $('.card').find(':button').prop('disabled', false);
                }
            }
        });
    }
    var ajaxTreeSample = function () {
        $("#tree_4").jstree({
            "core": {
                "themes": {
                    "responsive": false
                },
                // so that create works
                "check_callback": true,
                'data': {
                    'url': function (node) {
                        if (node.id == '#') {
                            var id = 'root';
                        } else {
                            var id = node.id;
                        }
                        var url = id;
                        if (typeof node.parents !== 'undefined' && node.parents.length > 0) {
                            $.each(node.parents, function (key, value) {
                                url += "+" + value;
                            });
                        }
                        return "/admin/export/search/" + url + "/";
                    }
                }
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state": {"key": "demo3"},
            "plugins": ["dnd", "state", "types", "checkbox"]
        });
    }
    var uploadFile = function () {

        var drop = $('#uploadFile');
        drop.dropzone({
            paramName: "file",
            uploadMultiple: false,
            parallelUploads: 1,
            maxFilesize: 8,
            maxFiles: 1,
            createImageThumbnails: false,
            acceptedFiles: ".ods",
            init: function () {
                this.on('addedfile', function (file) {
                    if (this.files.length > 1) {
                        this.removeFile(this.files[0]);
                    }
                });
                this.on('error', function (file, response) {

                });
                this.on("success", function (file, result) {
                    console.log(result);
                    if (result.success == true) {
                        toastr.success(result.msg);
                        $('#files > tbody').append(result.html);
                    } else {
                        toastr.error(result.error);
                    }
                });
            }
        });
    }
    var statistics = function () {
        if ($('#salesChart').length > 0) {
            $.ajax({
                url: '/admin/stats/',
                type: "GET",
                dataType: "json",
                cache: false,
                success: function (data) {
                    buildChart(data);
                }
            });
        }
    }
    var buildChart = function (data) {

        var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
        // This will get the first returned node in the jQuery collection.
        var salesChart = new Chart(salesChartCanvas);

        var salesChartData = {
            labels: data.months,
            datasets: [
                {
                    label: 'Rendelés',
                    fillColor: 'rgba(0, 123, 255, 0.9)',
                    strokeColor: 'rgba(0, 123, 255, 1)',
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(0, 123, 255, 1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(0, 123, 255, 1)',
                    data: data.results
                }
            ]
        }

        var salesChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: false,
            //String - Colour of the grid lines
            scaleGridLineColor: 'rgba(0,0,0,.05)',
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: false,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%=datasets[i].label%></li><%}%></ul>',
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' db' %>",
        }

        //Create the line chart
        salesChart.Line(salesChartData, salesChartOptions)
    }
    return {
        init: function () {
            verifyToken();
            setDatatableDefaults();
            backendProducts();
            backendUsers();
            backendPages();
            backendCoupons();
            backendDelivery();
            backendOrders();
            backendOffers();
            summernote();
            backendCategories();
            loadingButton();
            uploadFile();
            ajaxTreeSample();
            download();
            importFile();
            importStatus();
            statistics();
        }
    };
}();

$(function () {
    App.init();
});


