{
   "name": "New crud",
   "out": "app/Http/Controllers/Backend/{{ ucfirst($name) }}Controller.php",
   "params": {
         "name": "required"
   }
}
---
<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\{{ucfirst($name)}};
use App\Http\Requests\Backend\{{ucfirst($name)}}CreateRequest;
use App\Http\Requests\Backend\{{ucfirst($name)}}UpdateRequest;
use Cache;

class {{ucfirst($name)}}Controller extends Controller
{
    /**
     * Show {{str_plural($name)}}
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index({{ucfirst($name)}} $data)
    {
        return view('backend.{{str_plural($name)}}.index', compact('data'));
    }

    /**
     * Create {{str_plural($name)}}
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create({{ucfirst($name)}} $model)
    {
        return view('backend.{{str_plural($name)}}.form', compact('model'));
    }

    /**
     * Store {{str_plural($name)}}
     *
     * @param \App\Http\Controllers\Backend\{{ucfirst($name)}}CreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store({{ucfirst($name)}}CreateRequest $request)
    {
        if ($request->validated()) {
            $request->store();
            return back();
        }
    }

    /**
     * Edit {{str_plural($name)}}
     *
     * @param \App\Models\{{ucfirst($name)}} $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit({{ucfirst($name)}} $model,$id)
    {
        return view('backend.{{str_plural($name)}}.form', compact('model', 'id'));
    }

    /**
     * Update {{str_plural($name)}}
     *
     * @param \App\Http\Requests\Backend\{{ucfirst($name)}}UpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update({{ucfirst($name)}}UpdateRequest $request, $id)
    {
        if ($request->validated()) {
            $request->store($id);
            return back();
        }
    }

    /**
     * Destroy {{str_plural($name)}}
     *
     * @param \App\Models\{{ucfirst($name)}} $model
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy({{ucfirst($name)}} $model)
    {
        $model->delete();
        return back();
    }
}
