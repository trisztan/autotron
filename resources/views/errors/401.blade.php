@extends('errors::illustrated-layout')

@section('code', '401')
@section('title','401 Hiba')

@section('image')
<div style="background-image: url({{ asset('/svg/maintenance.jpg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection

@section('message', __('Bocs, az oldal megtekintéséhez nincs jogosultságod.'))
