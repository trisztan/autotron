@extends('errors::illustrated-layout')

@section('code', '503')
@section('title', __('Karbantartás'))

@section('image')
<div style="background-image: url({{ asset('/svg/maintenance.jpg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection

@section('message', __($exception->getMessage() ?: __('Bocs, Az oldal karbantartás alatt áll. Ez maximum 5 percet vesz igénybe.')))
