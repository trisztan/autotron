@extends('errors::illustrated-layout')

@section('code', '404')
@section('title', __('A keresett oldal nem található.'))

@section('image')
<div style="background-image: url({{ asset('/svg/maintenance.jpg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection

@section('message', __('A keresett oldal nem található.'))
