@extends('frontend.frontend')

@section('sidebar')
    @include('frontend.sidebar.dashboard')
@endsection

@section('content')

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-9 px-4 bg-white mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 pt-5">
                <h3>Bejelentkezés</h3>
                <form method="POST" action="{{ route('login') }}">
                @csrf
                    <!-- Email -->
                    <div class="form-group">
                        <label for="email" class="sr-only">E-mail cím</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                 <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </span>
                            <input  d="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail cím" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> <!-- / .form-group -->
                    <!-- Password -->
                    <div class="form-group">
                        <label for="password" class="sr-only">Jelszó</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-lock"></i></span>
                            </span>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Jelszó">
                        </div>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                        @endif
                    </div> <!-- / .form-group -->

                    <!-- Remember me -->
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" id="remember"  {{ old('remember') ? 'checked' : '' }}> Megjegyzés
                            </label>
                        </div>
                    </div> <!-- / .form-group -->
                    <!-- Submit -->
                    <button type="submit" class="btn btn-primary">Belépés</button>
                    <hr>
                </form>
                <!-- Sign up link -->
                <p>Nem regisztráltál még? <a href="sign-up.html">Regisztrálj most.</a></p>
                <p>Elfelejtetted a jelszavadat? <a href="">Kattints ide a visszaállításhoz</a>
                </p>
            </div>
        </div>
    </div>
</main>
@endsection
