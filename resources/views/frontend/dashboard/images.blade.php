@extends('frontend.frontend')

@section('sidebar')
    @include('frontend.sidebar.dashboard')
@endsection

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-9 px-4 bg-white mt-5 upload-form">
        <form id="uploadFile" action="/admin/import/upload" class="dropzone2" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="uuid" value="{{\str_random(20)}}">
            <div class="dz-default dz-message"><span><i class="fa fa-4x fa-cloud-upload" aria-hidden="true"></i><br/>Kattints ide a fájl feltöltéséhez</span></div>
            @csrf
        </form>
    </main>
@endsection
