@extends('frontend.frontend')

@section('sidebar')
    @include('frontend.sidebar.dashboard')
@endsection

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-9 px-4 bg-white mt-5 upload-form">
        <div id="upload" class="needs-validation mt-5" novalidate>
                <div class="row">
                    <div class="col-3">
                        <h4>Hirdetés feltöltés</h4>
                    </div>
                    <div class="col-6">
                        <div class="progress mt-2">
                            <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0"
                                 aria-valuemin="0" aria-valuemax="100">0%
                            </div>
                        </div>
                    </div>
                    <div class="col-3 text-right">
                        <button class="btn btn-primary d-inline-block" type="button" data-upload disabled>
                            <i class="fa fa-plus"></i> Hirdetés feltöltése
                        </button>
                    </div>
                </div>
                <hr class="line">
            <div class="alert alert-info">
                <i class="fa fa-info mr-3"></i> You can navigate beetween steps. Fill the input fields correctly, click to upload button in the right corner.
            </div>
                @foreach($groups->where('id',1) as $key => $group)
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        @php $i = 0; @endphp
                        @foreach($group->features->where('id','!=','1') as $key=> $feature)
                            @if($key>0)
                                <li class="nav-item">
                                    <a class="nav-link @if($key == 1) active @endif" id="{{str_slug($feature->title)}}-tab" data-toggle="tab" href="#{{str_slug($feature->title)}}"
                                       role="tab" aria-controls="{{str_slug($feature->title)}}" aria-selected="true"><span>{{$key}}</span> {{$feature->title}}</a>
                                </li>
                                @php $i++; @endphp
                            @endif
                        @endforeach
                        <li class="nav-item" style="display:none">
                            <a class="nav-link" id="images-tab" data-toggle="tab" href="#images"
                               role="tab" aria-controls="iamges" aria-selected="true"><span>{{ $i+1 }}</span> Képek</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        @php $checkKey = 0 @endphp
                        @php $i = 0; @endphp
                        @foreach($group->features->where('id','!=','1') as $key=> $feature)
                            @if($key>0)
                                @php $checkKey == $key; @endphp
                                <div class="tab-pane fade show @if($i == 0) active @endif" id="{{str_slug($feature->title)}}" role="tabpanel" aria-labelledby={{str_slug($feature->title)}}-tab">
                                @php $i++; @endphp
                            @endif
                            <div class="row">
                                @php $mode = 'create'; @endphp
                                @if($key==1)
                                    <div class="form-group col-12 mt-3">
                                        <div class="row">
                                            <div class="col-6 loadbrands">
                                                <label>Márka</label>
                                                <select class="form-control selectBrand" data-required="true" data-show="true" data-name="brand" name="brand" type="selectMulti">
                                                    <option value="0">Válassz egy Márkát</option>
                                                    @foreach($group->brands as $brand)
                                                        <option data-uuid="{{$brand->id}}" value="{{$brand->slug}}"
                                                                @if(isset($search['brands'][$brand->slug])) selected @endif>{{$brand->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-6">

                                                @foreach($group->brands as $brand)
                                                    <div class="models load{{$brand->slug}}"
                                                         @if(isset($search['brands'][$brand->slug]))  @else style="display:none" @endif>
                                                        <label>Model</label>
                                                        <select class="form-control selectModel" data-required="true" data-show='false' data-brand="{{$brand->slug}}"
                                                                data-brand-id="{{$brand->id}}" data-name="model" name="{{$brand->slug}}models"
                                                                type="selectMulti">
                                                            <option value="0">Válassz egy modelt</option>
                                                            @foreach($brand->models as $model)
                                                                <option data-uuid="{{$model->id}}" value="{{$model->slug}}"
                                                                        @if(isset($search['brands'][$brand->slug][$model->slug]))  selected @endif>{{$model->title}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @foreach($feature->attributes as $attribute)
                                    @include('frontend.search.'.$attribute->type,compact('attribute','mode'))
                                @endforeach
                            </div>
                            @if($key > 0 && $checkKey != $key)
                                </div>
                            @endif
                        @endforeach
                        <div class="tab-pane fade show" id="images" role="tabpanel" aria-labelledby=images-tab">
                            <form id="uploadFile" action="/admin/import/upload" class="dropzone2" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="uuid" value="{{\str_random(20)}}">
                                <div class="dz-default dz-message"><span><i class="fa fa-4x fa-cloud-upload" aria-hidden="true"></i><br/>Kattints ide a fájl feltöltéséhez</span></div>
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
    </main>
@endsection
@section('js')
    <script>var searchEnable = false;</script>
@endsection
