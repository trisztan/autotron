@extends('frontend.frontend')

@section('sidebar')
    @include('frontend.sidebar.dashboard')
@endsection
@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-9 px-4 bg-white">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content mt-5">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12  pt-5">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Autóim</h3>

                                    <div class="card-tools">
                                        <a href="{{ route('users.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Hirdetés feladás</a>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body p-1">
                                    <table id="vehiclesTable" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:120px;">#</th>
                                                <th>Típus</th>
                                                <th>Ár</th>
                                                <th>Nézettség</th>
                                                <th>Feltöltve</th>
                                                <th style="width:300px;">#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($vehicles as $vehicle)
                                            @php
                                                $price = $vehicle->getAttr('ar',false);
                                                $year = $vehicle->getAttr('ev',false);
                                                $engine = $vehicle->getAttr('uzemanyag',false);
                                                $km = $vehicle->getAttr('km',false);
                                                $cm3 = $vehicle->getAttr('cm3',false);
                                            @endphp
                                            <tr>
                                                <td><img style="width:100px" src="{{url($vehicle->getImage())}}"></td>
                                                <td>{{$vehicle->brand->title}} {{$vehicle->model->title}}<br/>
                                                    <span class="badge">{{$year}}</span>
                                                    <span class="badge">{{$engine}}</span>
                                                </td>
                                                <td>{!! Helper::money($price) !!}</td>
                                                <td>{!! $vehicle->views !!}</td>
                                                <td>{!! $vehicle->created_at !!}</td>
                                                <td class="text-center">
                                                    <div class="row pull-right mr-1">
                                                        <div class="d-inline mr-1">
                                                            <a href="{{route('users.edit',['id'=>$vehicle->id])}}"
                                                               class="btn btn-sm btn-info btn-block"><i class="fa fa-pencil"></i> Szerkesztés</a>
                                                        </div>
                                                        <div class="d-inline">
                                                            <form action="{{ route('users.destroy',['id'=>$vehicle->id]) }}"
                                                                  method="POST">
                                                                <input type="hidden" name="_method" value="DELETE">
                                                                <input type="hidden" name="_token"
                                                                       value="{{ csrf_token() }}">
                                                                <button type="submit"
                                                                        class="d-inline btn btn-sm btn-danger btn-block"><i
                                                                            class="fa fa-times"></i> Törlés</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </main>
@endsection

