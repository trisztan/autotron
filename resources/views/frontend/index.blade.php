@extends('frontend.frontend')

@section('sidebar')
    @include('frontend.sidebar.search')
@endsection

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-9 px-4 bg-white">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom" style="padding-top:80px;">
            <div class="btn-toolbar mb-2">
                <div class="col-2 mt-1">
                    <span style="font-weight: bold; text-transform: uppercase; font-size:12px;">Rendezés</span>
                </div>
                <div class="col-4">
                    <select class="form-control d-inline-block">
                        <option value="price.asc" selected>Ár (növekvő)</option>
                        <option value="price.desc">Ár (csökkenő)</option>
                        <option value="brand.asc">Márka (növekvő)</option>
                        <option value="brand.desc">Márka (csökkenő)</option>
                        <option value="year.asc">Évjárat (növekvő)</option>
                        <option value="year.desc">Évjárat (csökkenő)</option>
                    </select>
                </div>
                <div class="col-2 mt-2">
                    <span style="font-weight: bold; text-transform: uppercase;font-size:12px;">Megye</span>
                </div>
                <div class="col-4">
                    <select class="form-control col">
                        <option value="price.asc" selected>Ár (növekvő)</option>
                        <option value="price.desc">Ár (csökkenő)</option>
                        <option value="brand.asc">Márka (növekvő)</option>
                        <option value="brand.desc">Márka (csökkenő)</option>
                        <option value="year.asc">Évjárat (növekvő)</option>
                        <option value="year.desc">Évjárat (csökkenő)</option>
                    </select>
                </div>
                {{--<div class="col pull-right">
                    <button type="button" class="btn btn-outline-danger"><i class="fa fa-heart"></i> Keresés mentése</button>
                </div>--}}
            </div>
        </div>
        <div id="vehicles" data-cars></div>
    </main>
@endsection
@section('js')
<script>var searchEnable = true;</script>
@endsection
