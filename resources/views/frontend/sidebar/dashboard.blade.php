@if(Auth::user())
    <nav id="style-3" class="col-md-3 d-none d-md-block bg-light sidebar">
        <div class="sidebar-sticky">
            <h5 class="text-center">Hello, {{Auth::user()->name}}</h5>
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link {{request()->is('dashboard') ? 'active':''}}" href="{{route('dashboard')}}">Statisztika</a>
                <a class="nav-link {{request()->is('dashboard/upload') ? 'active':''}}"
                   href="{{route('dashboard.upload')}}">Hirdetés feltöltés</a>
                <a class="nav-link {{request()->is('dashboard/list') ? 'active':''}}"
                   href="{{route('dashboard.list')}}">Hirdetéseim</a>
                <a class="nav-link {{request()->is('dashboard/notifications') ? 'active':''}}"
                   href="{{route('dashboard.notifications')}}">Értesítések</a>
                <a class="nav-link {{request()->is('dashboard/settings') ? 'active':''}}"
                   href="{{route('dashboard.settings')}}">Személyes adatok</a>
            </div>
        </div>
    </nav>
@endif
