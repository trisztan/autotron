<nav  id="style-3" class="col-md-3 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <div class="tab-content" id="myTabContent">
            @if(isset($groups))
                @foreach($groups->where('id',1) as $key => $group)
                    <div class="tab-pane mt-3 fade show @if($key == 0) active @endif" id="{{$group->slug}}" role="tabpanel" aria-labelledby="home-tab">
                        <div class="form-group">
                            <div class="row">
                                <div class="col loadbrands">
                                    <label>Márka</label>
                                    <select class="form-control selectBrand" name="brand[]" multiple>
                                        @foreach($group->brands as $brand)
                                            <option data-uuid="{{$brand->id}}" value="{{$brand->slug}}" @if(isset($search['brands'][$brand->slug])) selected @endif>{{$brand->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @foreach($group->brands as $brand)
                                <div class="models load{{$brand->slug}}" @if(isset($search['brands'][$brand->slug]))  @else style="display:none" @endif>
                                    <label>{{$brand->title}} modellek</label>
                                    <select class="form-control selectModel" data-brand="{{$brand->slug}}" data-brand-id="{{$brand->id}}"  name="{{$brand->slug}}models[]" multiple>
                                        @foreach($brand->models as $model)
                                            <option data-uuid="{{$model->id}}" value="{{$model->slug}}" @if(isset($search['brands'][$brand->slug][$model->slug]))  selected @endif>{{$model->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endforeach
                        </div>
                        <div class="filter">
                            @foreach($group->features as $key=> $feature)
                                @if($key>1)
                                    <p class="text-center font-weight-bold">{{$feature->title}}</p>
                                @endif
                                <div class="row">
                                    @php $mode = 'search'; @endphp
                                    @foreach($feature->attributes->where('search','yes') as $attribute)
                                        @include('frontend.search.'.$attribute->searchType,compact('attribute','mode'))
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</nav>
