@extends('frontend.frontend')

@section('content')
    <div class="topic">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h3>{{$page->title}}</h3>
                </div>
                <div class="col-sm-8">
                    <ol class="breadcrumb pull-right hidden-xs">
                        <li><a href="{{ url('/') }}">Főoldal</a></li>
                        <li class="active">{{$page->title}}</li>
                    </ol>
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </div>
    <section class="shop-index__section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    @php
                        echo $page->description;
                    @endphp
                </div>
            </div> <!-- / .container -->
    </section>
@endsection
