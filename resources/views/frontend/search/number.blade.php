<div class="col-6">
    <div class="form-group mb-0">
        <div class="row">
            <div class="col">
                <label>{{$attribute->title}}</label>
                <div class="input-group mb-3">
                    <input type="number" class="form-control" data-required="{{$attribute->required}}" data-show="true" data-attribute="{{$attribute->id}}" value="0" name="{{$attribute->slug}}{{$mode == 'create' ? '':'_from'}}" id="{{$attribute->slug}}{{$mode == 'create' ? '':'_from'}}">
                    <div class="input-group-append">
                        <span class="input-group-text">{{$attribute->suffix}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
