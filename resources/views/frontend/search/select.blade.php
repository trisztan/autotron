<div class="col-6">
    <div class="form-group">
        <label>{{$attribute->title}}</label>
        <select type="select" class="form-control" data-required="{{$attribute->required}}" data-show="true" data-attribute="{{$attribute->id}}" name="{{$attribute->slug}}" id="{{$attribute->slug}}" data-required="{{$attribute->required}}">
            <option value="0">Nincs megadva</option>
            @foreach($attribute->values as $value)
                <option data-uuid="{{$value->id}}" value="{{$value->slug}}">{{$value->title}}</option>
            @endforeach
        </select>
    </div>
</div>
