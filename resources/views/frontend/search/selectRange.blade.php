<div class="col-12">
    <div class="form-group mb-0">
        <div class="row">
            <div class="col-6">
                <label>{{$attribute->title}}</label>
                <select type="select" data-show="true" data-attribute="{{$attribute->id}}" class="form-control" name="{{$attribute->slug}}{{$mode == 'create' ? '':'_from'}}" id="{{$attribute->slug}}{{$mode == 'create' ? '':'_from'}}">
                    <option value="0">Min</option>
                    @foreach($attribute->values->sortBy('slug') as $value)
                        <option data-uuid="{{$value->id}}" value="{{$value->slug}}">{{$value->title}}</option>
                    @endforeach
                </select>
            </div>
            @if($mode != 'create')
            <div class="col-6">
                <label>&nbsp;</label>
                <select type="select" data-attribute="{{$attribute->id}}" class="form-control" name="{{$attribute->slug}}_to" id="{{$attribute->slug}}_to">
                    <option value="0">Max</option>
                    @foreach($attribute->values as $value)
                        <option data-uuid="{{$value->id}}" value="{{$value->slug}}">{{$value->title}}</option>
                    @endforeach
                </select>
            </div>
            @endif
        </div>
    </div>
</div>
