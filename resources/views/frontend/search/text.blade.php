<div class="col-6">
    <div class="form-group">
        <label>{{$attribute->title}}</label>


        <div class="input-group mb-3">
            <input type="text" data-show="true" class="form-control" name="{{$attribute->slug}}">
            <div class="input-group-append">
                <span class="input-group-text">{{$attribute->suffix}}</span>
            </div>
        </div>
    </div>
</div>
