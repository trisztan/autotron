<div class="col-6">
    <div class="form-group">
        <label for="{{$attribute->slug}}">{{$attribute->title}}</label>
        <select type="selectmulti" data-required="{{$attribute->required}}" data-show="true" data-attribute="{{$attribute->id}}" class="form-control selectMulti"
                                                          id="{{$attribute->slug}}" name="{{$attribute->slug}}[]" multiple>
            @foreach($attribute->values as $value)
                <option data-uuid="{{$value->id}}" value="{{$value->slug}}">{{$value->title}}</option>
            @endforeach
        </select>
    </div>
</div>
