@foreach($vehicles as $vehicle)
    @php
        $price = $vehicle->getAttr('ar',false);
        $year = $vehicle->getAttr('ev',false);
        $engine = $vehicle->getAttr('uzemanyag',false);
        $km = $vehicle->getAttr('km',false);
        $cm3 = $vehicle->getAttr('cm3',false);
    @endphp

        <div class="item position-relative" data-href="{{url($vehicle->url)}}">
            <div class="image" style="background-image: url('{{url($vehicle->getImage())}}')"></div>
            <div class="details">
                <div class="price">
                    {!! Helper::money($price) !!}
                </div>
                <div class="info">
                    <span class="brand">{{$vehicle->brand->title}} {{$vehicle->model->title}}</span>
                    <span class="badge">{{$year}}</span>
                    <span class="badge">{{$engine}}</span>
                    <span class="badge">{{Helper::money($km)}}</span>
                    <span class="badge">{{Helper::money($cm3)}}</span>
                    <span class="id">
                        #{{$vehicle->uuid}}
                    </span>
                </div>
                <div class="save">
                    <a href="" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                </div>
            </div>
        </div>
@endforeach
