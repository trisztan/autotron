@include('frontend.header')
<div class="container-fluid mt-3">
    <div class="row">
        @yield('sidebar')
        @yield('content')
    </div>
</div>


<!-- JavaScript
================================================== -->
<script src="{{ mix('js/frontend/app.js') }}"></script>
@yield('js')

</body>
</html>
