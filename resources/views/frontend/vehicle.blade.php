@extends('frontend.frontend')

@section('sidebar')
    @include('frontend.sidebar.vehicle')
@endsection

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-9 px-4 bg-white" style="padding-top:100px;">
        <div class="row">
            <div class="col-4">
                @foreach($vehicle->images as $key => $image)
                    <img width="100%" src="{{Helper::image($image->path)}}">
                @endforeach
                <div class="image" style="background-image: url('{{url($vehicle->getImage())}}')"></div>
            </div>
            <div class="col-4">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col" colspan="2">Adatok</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td width="30%">Azonosító:</td>
                        <td>  #{{$vehicle->uuid}}</td>
                    </tr>
                    <tr>
                        <td scope="col" colspan="2">{{$vehicle->brand->title}} {{$vehicle->model->title}}</td>
                    </tr>
                    </tbody>
                </table>

                @foreach($vehicle->group->features as $key => $feature)
                    <table class="table table-bordered">
                        @if($key != 0)
                        <thead>
                        <tr>
                            <th scope="col" colspan="2">  {{$feature->title}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @endif
                        @foreach($feature->attributes as $attribute)

                            @foreach($vehicle->values as $value)
                                @if($value->attribute_id == $attribute->id)
                                    @if(empty($value->data))
                                        @php break; @endphp
                                    @endif
                                    @if(count($attribute->values) == 0)
                                        <tr>
                                            <td width="30%">{{$attribute->title}}</td>
                                            <td>{{$value->data}} {{$attribute->suffix ?? ''}}</td>
                                        </tr>
                                    @else
                                        @foreach($attribute->values as $selected)
                                            @if($selected->id == $value->data)
                                                <tr>
                                                    <td width="30%">{{$attribute->title}}</td>
                                                    <td>{{$selected->title}} {{$attribute->suffix ?? ''}}</td>
                                                </tr>
                                                @php break; @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        @endforeach
                        </tbody>
                        @endforeach
                    </table>
            </div>
            <div class="col-4">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col" colspan="2">Adatok</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td width="30%">Azonosító:</td>
                        <td>  #{{$vehicle->uuid}}</td>
                    </tr>
                    <tr>
                        <td scope="col" colspan="2">{{$vehicle->brand->title}} {{$vehicle->model->title}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </main>
@endsection

