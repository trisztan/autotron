<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ asset('frontend/ico/favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{config('app.name')}}</title>

    <!-- CSS Plugins -->
    <link href="{{ asset('frontend/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/plugins/animate/animate.css') }}" rel="stylesheet">

    <!-- CSS Global -->
    <link href="{{ mix('css/frontend/app.css') }}" rel="stylesheet" >
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>

</head>
<body>

<!-- NAVIGATION
================================================== -->
<nav class="navbar navbar-dark fixed-top d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal mb-3" data-href="{{url('/')}}">AutoTron.hu</h5>
    <span class="pull-left position-absolute mt-3" data-href="{{url('/')}}">ingyenes használtautó portál</span>
    <nav class="my-2 my-md-0 mr-md-3">

        @if(Auth::user())
            <a class="btn btn-secondary mr-3 btn-sm" href="{{route('dashboard')}}"><i class="fa fa-user"></i> Profilom</a>
            <a class="btn btn-secondary  mr-3 btn-sm" href="{{route('dashboard.upload')}}"><i class="fa fa-upload"></i> Hírdetés feltöltés</a>
            <a class="btn btn-sm btn-danger m-2" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-times"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @else
            <a class="btn btn-secondary btn-sm" href="{{route('login')}}"><i class="fa fa-sign-in"></i> Belépés</a>
            <a class="btn btn-secondary btn-sm" href="{{route('register')}}"><i class="fa fa-sign-in"></i> Regisztráció</a>
            <a class="btn btn-primary mr-3 btn-sm" href="#"><i class="fa fa-upload"></i> Hírdetés feltöltés</a>
        @endif
    </nav>
</nav>
