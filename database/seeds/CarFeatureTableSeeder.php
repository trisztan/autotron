<?php

use Illuminate\Database\Seeder;
use App\Models\Feature;

class CarFeatureTableSeeder extends Seeder
{
    public static function years()
    {
        return range(2010, 1900);
    }

    /**
     * Run the database seeds.
     */
    public function run()
    {
      /*  1. Ár
        2. Álatános adatok
        3. Műszaki adatok
        4. Váltó, hajtás
        5. Szín
        6. Külső felszereltség
      */
        $features[] = [
            'title'    => [
                'hu' => 'Márka',
                'en' => 'Brand'
            ],
            'icon'  => 'fa-car',
        ];
        $features[] = [
            'title'    => [
                'hu' => 'Vételár',
                'en' => 'Price'
            ],
            'icon'  => 'fa-car',
        ];

        $features[] = [
            'title'    => [
                'hu' => 'Általános adatok',
                'en' => 'Base informations'
            ],
            'icon'  => 'fa-car',
        ];

        $features[] = [
            'title'    => [
                'hu' => 'Műszaki adatok',
                'en' => 'Technical data'
            ],
            'icon'  => 'fa-car',
        ];

        $features[] = [
            'title'    => [
                'hu' => 'Felszereltség',
                'en' => 'Features'
            ],
            'icon'  => 'fa-car',
        ];

        foreach ($features as $key => $feature) {
            $featureSave = new Feature;
            $featureSave->setTranslation('title', 'hu', $feature['title']['hu']);
            $featureSave->setTranslation('title', 'en', $feature['title']['en']);
            $featureSave->group_id = 1;
            $featureSave->order = $key;
            $featureSave->save();
        }
    }
}
