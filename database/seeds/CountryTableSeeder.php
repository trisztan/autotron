<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    public function run()
    {
        DB::unprepared(file_get_contents(database_path('databases/countries.sql')));
    }
}
