<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    public function run()
    {
        DB::unprepared(file_get_contents(database_path('databases/states.sql')));
    }
}
