<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(UsersTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(BrandTableSeeder::class);
        $this->call(ModelTableSeeder::class);
        $this->call(GroupTableSeeder::class);
        $this->call(CarFeatureTableSeeder::class);
        $this->call(CarAttributeTableSeeder::class);
        $this->call(CarTableSeeder::class);
    }
}
