<?php

use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder
{
    public function run()
    {
        DB::unprepared(file_get_contents(database_path('databases/brands.sql')));
    }
}
