<?php

use Illuminate\Database\Seeder;

class ModelTableSeeder extends Seeder
{
    public function run()
    {
        DB::unprepared(file_get_contents(database_path('databases/models.sql')));
    }
}
