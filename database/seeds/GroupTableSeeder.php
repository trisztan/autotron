<?php

use Illuminate\Database\Seeder;
use App\Models\Group;

class GroupTableSeeder extends Seeder
{
    public static function years()
    {
        return range(2010, 1900);
    }

    /**
     * Run the database seeds.
     */
    public function run()
    {
        $groups[] = [
            'icon'  => 'fa-car',
            'title' => [
                'hu' => 'Autók',
                'en' => 'Cars',
                'at' => 'Auto'
            ],
            'slug'  => [
                'hu' => 'auto',
                'en' => 'cars',
                'at' => 'auto'
            ]
        ];

        $groups[] = [
            'icon'  => 'fa-motorcycle',
            'title' => [
                'hu' => 'Motor',
                'en' => 'Motor'
            ],
            'slug'  => [
                'hu' => 'motor',
                'en' => 'motor'
            ]
        ];

        $groups[] = [
            'icon'  => 'fa-ship',
            'title' => [
                'hu' => 'Hajó',
                'en' => 'Ship'
            ],
            'slug'  => [
                'hu' => 'hajo',
                'en' => 'ship'
            ]
        ];

        foreach ($groups as $key => $group) {
            $groupSave = new Group;
            $groupSave->setTranslation('title', 'hu', $group['title']['hu']);
            $groupSave->setTranslation('title', 'en', $group['title']['en']);
            $groupSave->setTranslation('slug', 'hu', $group['slug']['hu']);
            $groupSave->setTranslation('slug', 'en', $group['slug']['en']);
            $groupSave->icon = $group['icon'];
            $groupSave->order = $key;
            $groupSave->save();
        }
    }
}
