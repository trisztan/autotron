<?php

use Illuminate\Database\Seeder;
use App\Models\Attribute;
use App\Models\AttributeValue;

class CarAttributeTableSeeder extends Seeder
{
    public static function years()
    {
        return range(2010, 1900);
    }

    /**
     * Run the database seeds.
     */
    public function run()
    {
        /*  1. Márka */
        $attributes['type'] = [
            'group_id' => 1,
            'feature_id' => 1,
            'title' => [
                'hu' => 'Típusjel',
                'en' => 'Type',
                'at' => 'Freitext'
            ],
            'slug' => [
                'hu' => 'tipusjel',
                'en' => 'type',
                'at' => 'freitext'
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'text',
            'search' => 'yes',
            'searchType' => 'text',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 1,
            'values' => '',
        ];

        /*  2. Ár */
        $attributes['price'] = [
            'group_id' => 1,
            'feature_id' => 2,
            'title' => [
                'hu' => 'Vételár',
                'en' => 'Price',
                'at' => 'Preis'
            ],
            'slug' => [
                'hu' => 'ar',
                'en' => 'price',
                'at' => 'preis'
            ],
            'icon' => '',
            'suffix' => 'Ft',
            'type' => 'number',
            'search' => 'yes',
            'searchType' => 'numberRange',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 3,
            'values' => 'number'
        ];

        /* 3. Álatános adatok */
        $attributes['km'] = [
            'group_id' => 1,
            'feature_id' => 3,
            'title' => [
                'hu' => 'Kilométer',
                'en' => 'Kilometer',
                'at' => 'Kilometerstand'
            ],
            'slug' => [
                'hu' => 'km',
                'en' => 'km',
                'at' => 'ks'
            ],
            'icon' => '',
            'suffix' => 'km',
            'type' => 'number',
            'search' => 'yes',
            'searchType' => 'numberRange',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 3,
            'values' => 'number'
        ];

        $attributes['year'] = [
            'group_id' => 1,
            'feature_id' => 3,
            'title' => [
                'hu' => 'Évjárat',
                'en' => 'Year',
                'at' => 'Erstzulassung'
            ],
            'slug' => [
                'hu' => 'ev',
                'en' => 'year',
                'at' => 'erstzulassung'
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'select',
            'search' => 'yes',
            'searchType' => 'selectRange',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 4,
            'values' => 'years',
        ];
        $attributes['body'] = [
            'group_id' => 1,
            'feature_id' => 3,
            'title' => [
                'hu' => 'Kivitel',
                'en' => 'Body',
                'de' => 'Aufbauart',
                'at' => 'Aufbauart',
                'es' => 'Cuerpo',
                'ro' => 'Caroserie',
                'it' => 'Carrozzeria',
                'fr' => 'Carrosserie',
                'be' => '',
                'sk' => '',
                'si' => '',
                'ch' => '',
                'se' => ''
            ],
            'slug' => [
                'hu' => 'kivitel',
                'en' => 'body',
                'at' => 'aufbauart'
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'select',
            'search' => 'yes',
            'searchType' => 'selectMulti',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 5,
            'values' => [
                [
                    'title' => [
                        'hu' => 'Buggy',
                        'en' => 'Buggy',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Kabrio',
                        'en' => 'Cabrio',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Kupé',
                        'en' => 'Coupe',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Egyterű',
                        'en' => 'Monocab',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Ferdehátú',
                        'en' => 'hatchback',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Kisbusz',
                        'en' => 'Minibusz',
                    ],
                    'icon' => ''
                ],
                /* SHIT: kiegészíteni*/
            ]
        ];
        $attributes['condition'] = [
            'group_id' => 1,
            'feature_id' => 3,
            'title' => [
                'hu' => 'Állapot',
                'en' => 'Condition',
            ],
            'slug' => [
                'hu' => 'allapot',
                'en' => 'condition',
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'select',
            'search' => 'yes',
            'searchType' => 'select',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 6,
            'values' => [
                [
                    'title' => [
                        'hu' => 'Normál',
                        'en' => 'Normal',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Sérült',
                        'en' => 'Damaged',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Hiányos',
                        'en' => 'Defective',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Fődarab hibás',
                        'en' => 'Defective parts',
                    ],
                    'icon' => ''
                ]
            ]
        ];
        $attributes['doors'] = [
            'group_id' => 1,
            'feature_id' => 3,
            'title' => [
                'hu' => 'Ajtók száma',
                'en' => 'Doors',
            ],
            'slug' => [
                'hu' => 'ajto',
                'en' => 'doors',
            ],
            'icon' => '',
            'suffix' => 'ajtó',
            'type' => 'select',
            'search' => 'yes',
            'searchType' => 'selectMulti',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 7,
            'values' => [
                [
                    'title' => [
                        'hu' => '1 ajtó',
                        'en' => '1 door',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '2 ajtó',
                        'en' => '2 doors',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '3 ajtó',
                        'en' => '3 doors',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '4 ajtó',
                        'en' => '4 doors',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '5 ajtó',
                        'en' => '5 doors',
                    ],
                    'icon' => ''
                ],
            ],
        ];
        $attributes['persons'] = [
            'group_id' => 1,
            'feature_id' => 3,
            'title' => [
                'hu' => 'Ülések száma',
                'en' => 'Seats',
            ],
            'slug' => [
                'hu' => 'ules',
                'en' => 'seats',
            ],
            'icon' => '',
            'suffix' => 'személy',
            'type' => 'select',
            'search' => 'yes',
            'searchType' => 'selectMulti',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 8,
            'values' => [
                [
                    'title' => [
                        'hu' => '1 fő',
                        'en' => '1 person',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '2 fő',
                        'en' => '2 person',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '3 fő',
                        'en' => '3 person',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '4 fő',
                        'en' => '4 person',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '5 fő',
                        'en' => '5 person',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '6 fő',
                        'en' => '6 person',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '7 fő',
                        'en' => '7 person',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '8 fő',
                        'en' => '8 person',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => '9 fő',
                        'en' => '9 person',
                    ],
                    'icon' => ''
                ],
            ],
        ];

        /* 4. Műszaki adatok */
        $attributes['cm3'] = [
            'group_id' => 1,
            'feature_id' => 4,
            'title' => [
                'hu' => 'Cm³',
                'en' => 'Cm³',
            ],
            'slug' => [
                'hu' => 'cm3',
                'en' => 'cm3',
            ],
            'icon' => '',
            'suffix' => 'cm³',
            'type' => 'number',
            'search' => 'yes',
            'searchType' => 'numberRange',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 10,
            'values' => 'number'
        ];

        $attributes['fuel'] = [
            'group_id' => 1,
            'feature_id' => 4,
            'title' => [
                'hu' => 'Üzemanyag',
                'en' => 'Fuel',
                'at' => 'Treibstoff'
            ],
            'slug' => [
                'hu' => 'uzemanyag',
                'en' => 'fuel',
                'at' => 'treibstoff'
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'select',
            'search' => 'yes',
            'searchType' => 'select',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 11,
            'values' => [
                [
                    'title' => [
                        'hu' => 'Benzin',
                        'en' => 'Petrol',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Benzin & Gáz',
                        'en' => 'Petrol & Gas',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Biodízel',
                        'en' => 'Biodiesel',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Diesel',
                        'en' => 'Diesel',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Elektromos',
                        'en' => 'Electronic',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Etanol',
                        'en' => 'Ethanol',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Gáz',
                        'en' => 'Gas',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Hibrid (benzin)',
                        'en' => 'Hybrid (petrol)',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Hibrid (diesel)',
                        'en' => 'Hybrid (diesel)',
                    ],
                    'icon' => ''
                ],
            ]
        ];

        $attributes['engine'] = [
            'group_id' => 1,
            'feature_id' => 4,
            'title' => [
                'hu' => 'Hengerelrendezés',
                'en' => 'Engine Type',
            ],
            'slug' => [
                'hu' => 'hengerelrendezes',
                'en' => 'enginetype',
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'select',
            'search' => 'yes',
            'searchType' => 'selectMulti',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 12,
            'values' => [
                [
                    'title' => [
                        'hu' => 'Boxer',
                        'en' => 'Boxer',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Soros álló',
                        'en' => 'Soros fekvő',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'V',
                        'en' => 'V',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'W',
                        'en' => 'W',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Wankel',
                        'en' => 'Wankel',
                    ],
                    'icon' => ''
                ],
            ]
        ];

        $attributes['kw'] = [
            'group_id' => 1,
            'feature_id' => 4,
            'title' => [
                'hu' => 'Motor teljesítménye',
                'en' => 'Engine power',
            ],
            'slug' => [
                'hu' => 'motor_teljeseitmeny',
                'en' => 'enginepower',
            ],
            'icon' => '',
            'suffix' => 'kw',
            'type' => 'number',
            'search' => 'yes',
            'searchType' => 'numberRange',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 13,
            'values' => ''
        ];

        $attributes['weightself'] = [
            'group_id' => 1,
            'feature_id' => 4,
            'title' => [
                'hu' => 'Saját tömeg',
                'en' => 'Weight (just car)',
            ],
            'slug' => [
                'hu' => 'sajattomeg',
                'en' => 'selfweight',
            ],
            'icon' => '',
            'suffix' => 'kg',
            'type' => 'number',
            'search' => 'yes',
            'searchType' => 'numberRange',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 14,
            'values' => ''
        ];

        $attributes['weighttotal'] = [
            'group_id' => 1,
            'feature_id' => 4,
            'title' => [
                'hu' => 'Össztömeg',
                'en' => 'Total Weight',
            ],
            'slug' => [
                'hu' => 'ossztomeg',
                'en' => 'totalweight',
            ],
            'icon' => '',
            'suffix' => 'kg',
            'type' => 'number',
            'search' => 'yes',
            'searchType' => 'numberRange',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 15,
            'values' => ''
        ];

        $attributes['trunk'] = [
            'group_id' => 1,
            'feature_id' => 4,
            'title' => [
                'hu' => 'Csomagtartó',
                'en' => 'csomgtarto',
            ],
            'slug' => [
                'hu' => 'ossztomeg',
                'en' => 'trunk',
            ],
            'icon' => '',
            'suffix' => 'L',
            'type' => 'number',
            'search' => 'yes',
            'searchType' => 'numberRange',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 16,
            'values' => ''
        ];

        $attributes['gear'] = [
            'group_id' => 1,
            'feature_id' => 4,
            'title' => [
                'hu' => 'Sebességváltó',
                'en' => 'valto',
            ],
            'slug' => [
                'hu' => 'Gear',
                'en' => 'gear',
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'select',
            'search' => 'yes',
            'searchType' => 'select',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 17,
            'values' => [
                [
                    'title' => [
                        'hu' => 'Automata (3 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (4 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (5 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (6 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (7 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (8 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (9 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (10 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Félautomata sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Fokozatmentes automata sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Manuális (3 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Manuális (4 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Manuális (5 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Manuális (6 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Manuális (7 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Szekvenciális (4 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Szekvenciális (5 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Szekvenciális (6 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Szekvenciális (7 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Szekvenciális (8 fokozatú) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (4 fokozatú tiptronic) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (5 fokozatú tiptronic) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (6 fokozatú tiptronic) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (7 fokozatú tiptronic) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Automata (8 fokozatú tiptronic) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''],
                [
                    'title' => [
                        'hu' => 'Automata (9 fokozatú tiptronic) sebességváltó', 'en' => '',
                    ],
                    'icon' => ''
                ]
            ]
        ];

        $attributes['powertrain'] = [
            'group_id' => 1,
            'feature_id' => 4,
            'title' => [
                'hu' => 'Hajtás',
                'en' => 'hajtas',
            ],
            'slug' => [
                'hu' => 'Powertrain',
                'en' => 'powertrain',
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'select',
            'search' => 'yes',
            'searchType' => 'select',
            'required' => 'yes',
            'hidden' => 'yes',
            'order' => 18,
            'values' => [
                [
                    'title' => [
                        'hu' => 'Első kerék',
                        'en' => 'elso',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Első kerék',
                        'en' => 'hatso',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Összkerék',
                        'en' => 'osszkerek',
                    ],
                    'icon' => ''
                ],
                [
                    'title' => [
                        'hu' => 'Kapcsolható összkerék',
                        'en' => 'osszkerek',
                    ],
                    'icon' => ''
                ],
            ]
        ];

        $attributes['muszaki'] = [
            'group_id' => 1,
            'feature_id' => 5,
            'title' => [
                'hu' => 'Műszaki felszereltség',
                'en' => 'Technical equipment',
            ],
            'slug' => [
                'hu' => 'muszaki',
                'en' => 'technical',
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'selectMulti',
            'search' => 'yes',
            'searchType' => 'selectMulti',
            'required' => 'no',
            'hidden' => 'yes',
            'order' => 1,
            'values' => [
                ['title' => ['hu' => 'éjjellátó asszisztens','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'ABS (blokkolásgátló)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'ADS (adaptív lengéscsillapító)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'APS (parkolóradar)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'ARD (automatikus távolságtartó)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'ASR (kipörgésgátló)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'EBD/EBV (elektronikus fékerő-elosztó)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'EDS (elektronikus differenciálzár)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'ESP (menetstabilizátor)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'fékasszisztens','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'GPS nyomkövető','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'guminyomás-ellenőrző rendszer','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'holttér-figyelő rendszer','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'indításgátló (immobiliser)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'lejtmenet asszisztens','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'MSR (motorféknyomaték szabályzás)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'rablásgátló','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'sávtartó rendszer','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'sávváltó asszisztens','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'tábla-felismerő funkció','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'visszagurulás-gátló','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'állítható felfüggesztés','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'centrálzár','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'chiptuning','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'EDC (elektronikus lengéscsillapítás vezérlés)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'kerámia féktárcsák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'kulcsnélküli indítás','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'részecskeszűrő','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'riasztó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'sebességfüggő szervókormány','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'sperr differenciálmű','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'sportfutómű','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'szervokormány','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'távolságtartó tempomat','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'tempomat','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'elektronikus futómű hangolás','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'távolsági fényszóró asszisztens','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'tolatókamera','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'tolatóradar','en' => '',],'icon'  => ''],
            ]
        ];

        $attributes['belter'] = [
            'group_id' => 1,
            'feature_id' => 5,
            'title' => [
                'hu' => 'Beltér felszereltség',
                'en' => 'Indoor equipment',
            ],
            'slug' => [
                'hu' => 'belter',
                'en' => 'indoor',
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'selectMulti',
            'search' => 'yes',
            'searchType' => 'selectMulti',
            'required' => 'no',
            'hidden' => 'yes',
            'order' => 1,
            'values' => [
                ['title' => ['hu' => 'függönylégzsák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'hátsó oldal légzsák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'kikapcsolható légzsák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'oldallégzsák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'térdlégzsák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'utasoldali légzsák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'vezetőoldali légzsák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'beépített gyerekülés','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'bukócső','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'csomag rögzítő','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'hátsó fejtámlák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'ISOFIX rendszer','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'sebességváltó zár','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'full extra','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'állófűtés','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'fűthető kormány','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'fűthető ülés','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'álló helyzeti klíma','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'hűthető kartámasz','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'hűthető kesztyűtartó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'üléshűtés/szellőztetés','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'bőr belső','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'műbőr-kárpit','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'velúr kárpit','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'állítható combtámasz','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'állítható hátsó ülések','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'automatikusan sötétedő belső tükör','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'bőr-szövet huzat','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'deréktámasz','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'dönthető utasülések','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'elektromos ülésállítás utasoldal','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'elektromos ülésállítás vezetőoldal','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'elektromosan állítható fejtámlák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'faberakás','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'garázsajtó távirányító','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'középső kartámasz','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'masszírozós ülés','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'memóriás vezetőülés','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'multifunkciós kormánykerék','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'plüss kárpit','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'ülésmagasság állítás','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'állítható kormány','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'fedélzeti komputer','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'kormányváltó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'sportülések','en' => '',],'icon'  => ''],
            ]
        ];

        $attributes['kulter'] = [
            'group_id' => 1,
            'feature_id' => 5,
            'title' => [
                'hu' => 'Kültér felszereltség',
                'en' => 'Outdoor equipment',
            ],
            'slug' => [
                'hu' => 'kulter',
                'en' => 'outdoor',
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'selectMulti',
            'search' => 'yes',
            'searchType' => 'selectMulti',
            'required' => 'no',
            'hidden' => 'yes',
            'order' => 1,
            'values' => [
                ['title' => ['hu' => 'gyalogos légzsák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'bi-xenon fényszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'bukólámpa','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'fényszóró magasságállítás','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'fényszórómosó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'kanyarkövető fényszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'kiegészítő fényszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'ködlámpa','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'LED fényszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'xenon fényszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'defekttűrő abroncsok','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'esőszenzor','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'fűthető ablakmosó fúvókák','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'fűthető szélvédő','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'ajtószervó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'automatikus csomagtér-ajtó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'automatikusan sötétedő külső tükör','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'elektromosan behajtható külső tükrök','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'pótkerék','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'defektjavító készlet','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'elektromos ablak elöl','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'elektromos ablak hátul','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'elektromos tolótető','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'elektromos tükör','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'fűthető tükör','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'könnyűfém felni','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'króm felni','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'színezett üveg','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'tolóajtó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'tolótető (napfénytető)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'vonóhorog','en' => '',],'icon'  => '']
            ]
        ];

        $attributes['multimedia'] = [
            'group_id' => 1,
            'feature_id' => 5,
            'title' => [
                'hu' => 'Multimédia felszereltség',
                'en' => 'Multimedia equipment',
            ],
            'slug' => [
                'hu' => 'multimedia',
                'en' => 'multimedia',
            ],
            'icon' => '',
            'suffix' => '',
            'type' => 'selectMulti',
            'search' => 'yes',
            'searchType' => 'selectMulti',
            'required' => 'no',
            'hidden' => 'yes',
            'order' => 1,
            'values' => [
                ['title' => ['hu' => 'autótelefon','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'CD-s autórádió','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'DVD','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'GPS (navigáció)','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'HIFI','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'rádió','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'rádiós magnó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'TV','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '1 DIN','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '2 DIN','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'CD tár','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'MP3 lejátszás','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'MP4 lejátszás','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'WMA lejátszás','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'analóg TV tuner','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'AUX csatlakozó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'bluetooth-os kihangosító','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'DVB tuner','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'DVB-T tuner','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'erősítő kimenet','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'FM transzmitter','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'HDMI bemenet','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'iPhone/iPod csatlakozó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'kihangosító','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'memóriakártya-olvasó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'merevlemez','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'mikrofon bemenet','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'tolatókamera bemenet','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'USB csatlakozó','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'érintőkijelző','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'erősítő','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'fejtámlamonitor','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'gyári erősítő','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'kormányra szerelhető távirányító','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'távirányító','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'tetőmonitor','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '2 hangszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '4 hangszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '5 hangszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '6 hangszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '7 hangszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '8 hangszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '9 hangszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '10 hangszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '11 hangszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => '12 hangszóró','en' => '',],'icon'  => ''],
                ['title' => ['hu' => 'mélynyomó','en' => '',],'icon'  => ''],

            ]
        ];

        foreach ($attributes as $attribute) {
            $attributeSave = new Attribute;
            $attributeSave->group_id = $attribute['group_id'];
            $attributeSave->feature_id = $attribute['feature_id'];
            $attributeSave->setTranslation('title', 'hu', $attribute['title']['hu']);
            $attributeSave->setTranslation('title', 'en', $attribute['title']['en']);
            $attributeSave->setTranslation('slug', 'hu', $attribute['slug']['hu']);
            $attributeSave->setTranslation('slug', 'en', $attribute['slug']['en']);
            $attributeSave->icon = $attribute['icon'];
            $attributeSave->type = $attribute['type'];
            $attributeSave->suffix = $attribute['suffix'];
            $attributeSave->search = $attribute['search'];
            $attributeSave->searchType = $attribute['searchType'];
            $attributeSave->required = $attribute['required'];
            $attributeSave->hidden = $attribute['hidden'];
            $attributeSave->order = $attribute['order'];
            $attributeSave->save();

            if (is_array($attribute['values'])) {
                foreach ($attribute['values'] as $value) {
                    $attributeValue = new AttributeValue;
                    $attributeValue->feature_id = $attributeSave->feature_id;
                    $attributeValue->attribute_id = $attributeSave->id;
                    $attributeValue->setTranslation('title', 'hu', $value['title']['hu']);
                    $attributeValue->setTranslation('title', 'en', $value['title']['en']);
                    $attributeValue->setTranslation('slug', 'hu', str_slug($value['title']['hu']));
                    $attributeValue->setTranslation('slug', 'en', str_slug($value['title']['en']));
                    $attributeValue->icon = $value['icon'];
                    $attributeValue->save();
                }
            }
            if ($attribute['values'] == 'years') {
                foreach ($this->years() as $value) {
                    $attributeValue = new AttributeValue;
                    $attributeValue->feature_id = $attributeSave->feature_id;
                    $attributeValue->attribute_id = $attributeSave->id;
                    $attributeValue->title = $value;
                    $attributeValue->slug = str_slug($value);
                    $attributeValue->save();
                }
            }

        }

    }
}
