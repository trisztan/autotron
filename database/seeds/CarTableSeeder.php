<?php

use App\Models\Attribute;
use App\Models\Vehicle;
use Illuminate\Database\Seeder;
use App\Models\Image;
use App\Models\VehicleValues;

class CarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $attributes = Attribute::whereGroupId(1)->get();
        $faker = Faker\Factory::create();
        factory(Vehicle::class)->times(100)->create()->each(function ($vehicle) use($faker,$attributes) {
            $vehicle->images()->saveMany(factory(Image::class, 2)->make());
            $data = null;
            foreach ($attributes as $attribute) {
                if (count($attribute->values) > 0) {
                    $data = $faker->randomElement($attribute->values->pluck('id'));
                }
                else {
                    if($attribute->slug == 'ar'){
                        $data = round(rand(100000,200000000),-4);
                    }
                    if($attribute->slug == 'ev'){
                        $data = rand(1990,2020);
                    }
                    if($attribute->slug == 'km'){
                        $data = round(rand(100,200000),-4);
                    }
                    if($attribute->slug == 'cm3'){
                        $data = round(rand(900,4000),-2);
                    }
                    if($attribute->slug == 'tipus'){
                        $data = $faker->randomElement(['si','gti','gt','xs','st']);
                    }
                }
                VehicleValues::create([
                    'attribute_id' => $attribute->id,
                    'vehicle_id' => $vehicle->id,
                    'data' => $data
                ]);
            }
        });
    }
}
