<?php

use Image as ImageSave;
use App\Models\Image as Image;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Image::class, function (Faker $faker) {

    $images = glob(database_path()."/seeds/images/*.jpg");
    $image = $faker->randomElement($images);
    $name = strtolower(str_random(5));
    $img = ImageSave::make(file_get_contents($image));
    $img->resize(1000,1000);
    $name = $name.'.jpg';
    $path = storage_path('app/public/'.$name);
    $img->save($path);

    return [
        'path' => $name,
        'url' => $name,
        'default' =>false
    ];
});
