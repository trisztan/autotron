<?php

use App\Models\Attribute;
use App\Models\VehicleValues;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(VehicleValues::class, function (Faker $faker) {
    $save = [];
    $attributes = Attribute::whereGroupId(1)->get();
    foreach($attributes as $attribute)
    {

        if(count($attribute->values)>0){
            $data = $faker->randomElement($attribute->values->pluck('id'));
        }
        else {
            $data = null;
        }

        $save[] = [
            'attribute_id'=>$attribute->id,
            'data'=> $data
        ];
    }

    return $save;
});
