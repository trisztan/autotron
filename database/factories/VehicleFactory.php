<?php

use App\Models\Brand;
use App\Models\Vehicle;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Vehicle::class, function (Faker $faker) {

    $brand = Brand::all()->random();
    $title = $brand->title. " eladó autó";

    return [
        'uuid'=> strtolower(Str::random(6)),
        'group_id' => 1,
        'brand_id' => $brand->id,
        'model_id' => $faker->randomElement($brand->models->pluck('id')),
        'title' => $title,
        'slug' => str_slug($title),
        'created_by' => rand(1,50),
    ];
});
