<?php

use Faker\Generator as Faker;
use App\Models\User;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $isCompany = rand(0,1);
    $isCompanyType = $faker->randomElement(['Kft','Bt']);

    return [
        'name' => $faker->name,
        'firstname' => $faker->name,
        'lastname' => $faker->name,
        'mobile' => $faker->e164PhoneNumber(),
        'is_admin' => 0,
        'is_company' => $isCompany,
        'company' => ($isCompany == 1 ? $faker->name.'.'.$isCompanyType:''),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
