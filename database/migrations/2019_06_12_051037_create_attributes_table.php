<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id');
            $table->integer('feature_id');
            $table->json('title');
            $table->json('slug');
            $table->string('icon')->nullable();
            $table->string('suffix')->nullable();
            $table->enum('type',['checkbox','select','number','date','text','selectmulti']);
            $table->enum('search',['yes','no']);
            $table->string('searchType');
            $table->enum('required',['yes','no']);
            $table->enum('hidden',['yes','no']);
            $table->string('callback')->nullable();
            $table->integer('order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
