<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('group_id');
            $table->integer('brand_id');
            $table->integer('model_id');
            $table->string('title');
            $table->string('slug');
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->default(0)->nullable();
            $table->timestamp('enabled_expire')->nullable();
            $table->tinyInteger('featured')->default(0)->nullable();
            $table->integer('created_by');
            $table->timestamp('featured_expire')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
