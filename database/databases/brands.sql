INSERT INTO `brands` (`id`, `title`, `slug`,`group_id`) VALUES
('1', 'Acura', 'acura',1),
('2', 'Alfa Romeo', 'alfa-romeo',1),
('3', 'Aptera', 'aptera',1),
('4', 'Aston Martin', 'aston-martin',1),
('5', 'Audi', 'audi',1),
('6', 'Austin', 'austin',1),
('7', 'Bentley', 'bentley',1),
('8', 'BMW', 'bmw',1),
('9', 'Bugatti', 'bugatti',1),
('10', 'Buick', 'buick',1),
('11', 'Cadillac', 'cadillac',1),
('12', 'Chevrolet', 'chevrolet',1),
('13', 'Chrysler', 'chrysler',1),
('14', 'Citroën', 'citroen',1),
('15', 'Corbin', 'corbin',1),
('16', 'Daewoo', 'daewoo',1),
('17', 'Daihatsu', 'daihatsu',1),
('18', 'Dodge', 'dodge',1),
('19', 'Eagle', 'eagle',1),
('20', 'Fairthorpe', 'fairthorpe',1),
('21', 'Ferrari', 'ferrari',1),
('22', 'FIAT', 'fiat',1),
('23', 'Fillmore', 'fillmore',1),
('24', 'Foose', 'foose',1),
('25', 'Ford', 'ford',1),
('26', 'Geo', 'geo',1),
('27', 'GMC', 'gmc',1),
('28', 'Hillman', 'hillman',1),
('29', 'Holden', 'holden',1),
('30', 'Honda', 'honda',1),
('31', 'HUMMER', 'hummer',1),
('32', 'Hyundai', 'hyundai',1),
('33', 'Infiniti', 'infiniti',1),
('34', 'Isuzu', 'isuzu',1),
('35', 'Jaguar', 'jaguar',1),
('36', 'Jeep', 'jeep',1),
('37', 'Jensen', 'jensen',1),
('38', 'Kia', 'kia',1),
('39', 'Lamborghini', 'lamborghini',1),
('40', 'Land Rover', 'land-rover',1),
('41', 'Lexus', 'lexus',1),
('42', 'Lincoln', 'lincoln',1),
('43', 'Lotus', 'lotus',1),
('44', 'Maserati', 'maserati',1),
('45', 'Maybach', 'maybach',1),
('46', 'Mazda', 'mazda',1),
('47', 'McLaren', 'mclaren',1),
('48', 'Mercedes-Benz', 'mercedes-benz',1),
('49', 'Mercury', 'mercury',1),
('50', 'Merkur', 'merkur',1),
('51', 'MG', 'mg',1),
('52', 'MINI', 'mini',1),
('53', 'Mitsubishi', 'mitsubishi',1),
('54', 'Morgan', 'morgan',1),
('55', 'Nissan', 'nissan',1),
('56', 'Oldsmobile', 'oldsmobile',1),
('57', 'Panoz', 'panoz',1),
('58', 'Peugeot', 'peugeot',1),
('59', 'Plymouth', 'plymouth',1),
('60', 'Pontiac', 'pontiac',1),
('61', 'Porsche', 'porsche',1),
('62', 'Ram', 'ram',1),
('63', 'Rambler', 'rambler',1),
('64', 'Renault', 'renault',1),
('65', 'Rolls-Royce', 'rolls-royce',1),
('66', 'Saab', 'saab',1),
('67', 'Saturn', 'saturn',1),
('68', 'Scion', 'scion',1),
('69', 'Shelby', 'shelby',1),
('70', 'Smart', 'smart',1),
('71', 'Spyker', 'spyker',1),
('72', 'Spyker Cars', 'spyker-cars',1),
('73', 'Studebaker', 'studebaker',1),
('74', 'Subaru', 'subaru',1),
('75', 'Suzuki', 'suzuki',1),
('76', 'Tesla', 'tesla',1),
('77', 'Toyota', 'toyota',1),
('78', 'Volkswagen', 'volkswagen',1),
('79', 'Volvo', 'volvo',1);
