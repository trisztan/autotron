<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// Backend routes
Route::get('admin/login','Backend\AdminController@login')->name('admin.login');
Route::prefix('admin')->middleware(['auth','web','admin'])->namespace('Backend')->group(function () {
    Route::get('stats','AdminController@stats');
    Route::get('/','AdminController@index');
    Route::resource('users','UserController')->except('show');
    Route::resource('pages','PageController')->except('show');
    Route::resource('orders','OrderController')->except('show');
    Route::get('order/remove/{order_id}/{sku}','OrderController@productRemove');
    Route::resource('offers','OfferController')->except('show');
    Route::resource('coupons','CouponController')->except('show');
    Route::resource('redirects','RedirectController')->except('show');
    Route::resource('imports','ImportController')->except('show');
});

// Frontend Routes
Route::namespace('Frontend')->middleware(['web'])->group(function () {
    Route::get('/','IndexController@index');
    Route::post('models','IndexController@models');
    Route::post('vehicles','IndexController@vehicles');
    Route::get('vehicle/{uuid}','IndexController@vehicle');
    Route::prefix('dashboard')->middleware(['auth','web','admin'])->group(function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::get('upload', 'DashboardController@upload')->name('dashboard.upload');
        Route::get('list', 'DashboardController@list')->name('dashboard.list');
        Route::get('images/{uuid}', 'DashboardController@images')->name('dashboard.images');
        Route::post('store', 'DashboardController@store')->name('dashboard.store');
        Route::get('notifications', 'DashboardController@notifications')->name('dashboard.notifications');
        Route::get('settings', 'DashboardController@settings')->name('dashboard.settings');
    });
    Route::get('images/{image}','IndexController@image');
    Route::any('{catchall}', 'IndexController@index')->where('catchall', '.*');
});
